# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

#############################################################
#	(C) Krzysztof Solek, Mielec 2012
#############################################################

import bpy
###########################################################
#
#       Global variables
#
###########################################################
obj_names=[]    # names of meshes in "C-suitable" format
vtx = []      # list of dictionaries for each mesh
faces = []    # list of lists
vl = []       # list of vertices for each mesh
nl = []       # list of normals for each mesh
uvl =   []    # list of UV coords for each mesh
obj_mtx=[]  # list of local transformations for each object
obj_cnt =   0   # object count
max_vcnt=   0   # qty of vertices for biggest mesh

###########################################################
#
#   Round values of the 3D vector
#
###########################################################

def r3d(v):
    return round(v[0],6), round(v[1],6), round(v[2],6)

###########################################################
#
#   Round values of the 2D vector
#
###########################################################

def r2d(v):
    return round(v[0],6), round(v[1],6)


###########################################################
#
#   Convert object name to be suitable for C definition
#
###########################################################

def clearName(name):
    tmp=name.upper()
    ret=""
    for i in tmp:
        if (i in " ./\-+#$%^!@"):
            ret=ret+"_"
        else:
            ret=ret+i
    return ret


###########################################################
#
#   Build data for each object (MESH)
#
###########################################################

def buildData (obj, msh, name):
    global obj_cnt
    global obj_names     # names of meshes in "C-suitable" format
    global vtx           # list of dictionaries for each mesh
    global faces         # list of lists
    global vl            # list of vertices for each mesh
    global nl            # list of normals for each mesh
    global uvl          # list of UV coords for each mesh
    global obj_mtx      # list of local transformations for each object

    lvdic = {} # local dictionary
    lfl = [] # lcoal faces index list
    lvl = [] # local vertex list
    lnl = [] # local normal list
    luvl = [] # local uv list
    lvcnt = 0 # local vertices count
    isSmooth = False
    hasUV = True    # true by default, it will be verified below
    
    print("Building for: %s\n"%obj.name)

    if (len(msh.tessface_uv_textures)>0):
        if (msh.tessface_uv_textures.active is None):
            hasUV=False
    else:
        hasUV = False
     
    if (hasUV):
        activeUV = msh.tessface_uv_textures.active.data
        
    obj_names.append(clearName(name))
    obj_cnt+=1
    
    for i,f in enumerate(msh.tessfaces):
        isSmooth = f.use_smooth
        tmpfaces = []
        for j,v in enumerate(f.vertices):  
            vec = msh.vertices[v].co
            vec = r3d(vec)
            
            if (isSmooth):  # use vertex normal
                nor = msh.vertices[v].normal
            else:           # use face normal
                nor = f.normal
            
            nor = r3d(nor)
            
            if (hasUV):
                co = activeUV[i].uv[j]
                co = r2d(co)
            else:
                co = (0.0, 0.0)
            
            key = vec, nor, co
            vinx = lvdic.get(key)
            
            if (vinx is None): # vertex not found
                lvdic[key] = lvcnt
                lvl.append(vec)
                lnl.append(nor)
                luvl.append(co)
                tmpfaces.append(lvcnt)
                lvcnt+=1
            else:
                inx = lvdic[key]
                tmpfaces.append(inx)
        
        if (len(tmpfaces)==3): 
            lfl.append(tmpfaces)
        else:
            lfl.append([tmpfaces[0], tmpfaces[1], tmpfaces[2]])
            lfl.append([tmpfaces[0], tmpfaces[2], tmpfaces[3]])

    
    #update global lists and dictionaries
    vtx.append(lvdic)        
    faces.append(lfl)
    vl.append(lvl)
    nl.append(lnl)
    uvl.append(luvl)
    obj_mtx.append(obj.matrix_local)

###########################################################
#
#   Write names of all exported objects to C file.
#   Each name is a index which can be used to refer
#   to values of single object
#
###########################################################

def writeAllObjectNames(file):
    for i,n in enumerate(obj_names):
        file.write("#define %s %d\n"%(n,i))

###########################################################
#
#   Write tranfsormations matrix (4x4) float values
#   into single table
#
###########################################################
        
def writeTransformations (file):
    file.write ("/***************************************\n")
    file.write (" *          local transformations\n")
    file.write (" ***************************************/\n\n")
    file.write ("float transformations[][16]={\n")
    for m in obj_mtx:
        file.write("\t{%ff, %ff, %ff, %ff, "%tuple(m.col[0]))
        file.write("%ff, %ff, %ff, %ff, "%tuple(m.col[1]))
        file.write("%ff, %ff, %ff, %ff, "%tuple(m.col[2]))
        file.write("%ff, %ff, %ff, %ff},\n "%tuple(m.col[3]))
    file.write("};\n")

###########################################################
#
#   Write all vertices to single table
#
###########################################################
 
def writeVertices(file):
    
    file.write ("/***************************************\n")
    file.write (" *\tvertices definition\n")
    file.write (" ***************************************/\n")
    
    file.write ("\nstruct vertex_struct {\n\tfloat x,y,z;\n\tfloat nx,ny,nz;\n\tfloat u,v;\n};\n")
    
 
    
    #write verictes table
    file.write ("struct vertex_struct vertices[]={\n")
    for i, d in enumerate(vl):
        file.write ("\t/* %s: %d vertices */\n"%(obj_names[i],len(d)))
        for j in range(0,len(d)):
            file.write ("\t{%ff, %ff, %ff, "%tuple(vl[i][j]))
            file.write ("%ff, %ff, %ff, "%tuple(nl[i][j]))
            file.write ("%ff, %ff},\n"%tuple(uvl[i][j]))
    file.write("};\n\n")


###########################################################
#
#   Write faces indexes into single table
#
###########################################################
    
def writeFaces(file):     
    if (max_vcnt<=65535):
        file.write("#define INX_TYPE GL_UNSIGNED_SHORT\n")
        file.write("unsigned short indexes[]={\n")
    else:
        file.write("#define INX_TYPE GL_UNSIGNED_INT\n")
        file.write("unsigned int indexes[]={\n")
    

    for i in range(len(faces)):
        file.write("/* %s %d faces */\n"%(obj_names[i], len(faces[i])))
        for j, f in enumerate(faces[i]):
            file.write ("%d, %d, %d, "%tuple(f)) 
        file.write("\n")
    file.write("};\n")


###########################################################
#
#   Write number of vertices and faces for each object
#   and generates offset tables for each object
#
###########################################################

def writeVertexAndFacesCountTables(file):
    global max_vcnt

    file.write ("/***************************************\n")
    file.write (" *\tfaces count for each mesh \n")
    file.write (" ***************************************/\n")
    file.write ("unsigned int faces_count[]={")
    for i,f in enumerate(faces):
        file.write ("%d"%len(f))
        if (i<(len(faces)-1)):
            file.write(", ")
        else:
            file.write("};\n\n")

    file.write ("/***************************************\n")
    file.write (" *\tvertices count for each mesh \n")
    file.write (" ***************************************/\n")
    file.write ("unsigned int vertex_count[]={")
    vlsize = len(vl)
    for i, list in enumerate(vl):
        file.write ("%d"%len(list))
        if (max_vcnt<len(list)):
            max_vcnt=len(list)
            
        if (i<(vlsize-1)):
            file.write (", ")
    file.write ("};\n\n")
    
    file.write ("/***************************************\n")
    file.write (" *\toffset tables for each mesh  \n")
    file.write (" ***************************************/\n")
    file.write ("unsigned int vertex_offset_table []={\n\t");
    voffset = 0
    for i in vl:
        file.write ("%d, "%voffset)
        voffset+=len(i)
    file.write("\n};\n")
    
    file.write ("unsigned int indices_offset_table []={\n\t")
    foffset=0
    for f in faces:
        file.write ("%d, "%foffset)
        foffset+=(len(f)*3)
    file.write("\n};\n")

###########################################################
#
#       Save data to C header file
#
###########################################################
    
def save(filename):
    file = open(filename, "w", newline="\n")
    file.write("#ifndef _BLENDER_EXPORT_H_\n#define _BLENDER_EXPORT_H_\n\n")
    file.write("#define OBJECTS_COUNT %d\n"%obj_cnt)
    writeAllObjectNames(file)
    writeTransformations (file)
    writeVertexAndFacesCountTables(file)
    writeVertices(file)
    writeFaces(file)
        
    file.write ("#endif")   
    file.close()
###########################################################
#
#       Export MESH object. By default export whole scene
#
###########################################################

def export(filename="untitled.h", entire_scene=True):
    global obj_cnt
    global obj_names     # names of meshes in "C-suitable" format
    global vtx           # list of dictionaries for each mesh
    global faces         # list of lists
    global vl            # list of vertices for each mesh
    global nl            # list of normals for each mesh
    global uvl          # list of UV coords for each mesh
    global obj_mtx      # list of local transformations for each object

    print("--------------------------------------------------\n")
    print("Starting script:\n")
    print(filename)

    # clear all gloabl variables
    obj_names=[]    # names of meshes in "C-suitable" format
    vtx = []      # list of dictionaries for each mesh
    faces = []    # list of lists
    vl = []       # list of vertices for each mesh
    nl = []       # list of normals for each mesh
    uvl =   []    # list of UV coords for each mesh
    obj_mtx=[]  # list of local transformations for each object
    obj_cnt =   0   # object count
    max_vcnt=   0   # qty of vertices for biggest mesh


    sc = bpy.context.scene  # export MESHes from active scene

    if (entire_scene):
        for o in sc.objects:
            if (o.type=="MESH"):    # export ONLY meshes
                msh = o.to_mesh(sc,True,"PREVIEW") # prepare MESH
                buildData(o, msh, o.name)
                bpy.data.meshes.remove(msh)
    else:
        o = sc.objects.active
        msh = o.to_mesh(sc,True,'PREVIEW')
        buildData(o, msh, o.name)
        bpy.data.meshes.remove(msh)

    save(filename)    
    print("Done\n")
    return {'FINISHED'}


