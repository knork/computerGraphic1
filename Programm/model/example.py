import bpy

obj = bpy.data.objects['Cube']
mesh = obj.data

normals = []
indices = []
for face in mesh.polygons:
    indices.append(face.vertices[0])
    indices.append(face.vertices[1])
    indices.append(face.vertices[2])
    for i in range(len(face.vertices)):
        v = mesh.vertices[face.vertices[i]]
        normals.append([v.normal[0],v.normal[1],v.normal[2]])

verts = []
for vert in mesh.vertices:
    verts.append(vert.co.xyz)

uvs = []
for uv_layer in mesh.uv_layers:
    for x in range(len(uv_layer.data)):
        uvs.append(uv_layer.data[x].uv)

print(indices)
print(verts)
print(uvs)
print(normals)