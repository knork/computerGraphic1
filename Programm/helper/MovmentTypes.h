//
// Created by Knork on 09.07.2016.
//

#ifndef CP16_MOVMENTTYPES_H
#define CP16_MOVMENTTYPES_H

enum MovementDirections{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

#endif //CP16_MOVMENTTYPES_H
