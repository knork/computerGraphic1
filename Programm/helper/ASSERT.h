//
// Created by Knork on 03.07.2016.
// collective copying from many sites like stack overflow etc.
// http://stackoverflow.com/questions/21957498/custom-assert-macro-that-supports-commas-and-error-message
// http://www.cplusplus.com/reference/cassert/assert/
// http://stackoverflow.com/questions/5252375/custom-c-assert-macro
// http://cnicholson.net/2009/02/stupid-c-tricks-adventures-in-assert/
//

#ifndef CP16_ASSERT
#define CP16_ASSERT

#include <iostream>

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << std::endl << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

#ifndef NDEBUG
#   define DATA_ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << std::endl << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

#endif