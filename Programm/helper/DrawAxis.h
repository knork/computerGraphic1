//
// Created by Knork on 19.07.2016.
//

#ifndef CP16_DRAWAXIS_H
#define CP16_DRAWAXIS_H


#include <GL/glew.h>
#include <glm/vec3.hpp>

class DrawAxis{

    Shader *shader;

public:
    DrawAxis(){
        shader = new Shader("shader/vAxisShader.glsl","shader/fAxisShader.glsl");
        setupMesh();
    }
    ~DrawAxis(){
        delete shader;
    }

    void draw(){
        // draw mesh
        shader->use();
        glBindVertexArray(this->VAO);
        glLineWidth(2.0f);
        glDrawArrays(GL_LINES, 0,6 );
        glLineWidth(1.0f);
        glBindVertexArray(0);
    }

private:



    GLuint VAO, VBO;
    void setupMesh()
    {

        glm::vec3 vertices[12]={glm::vec3(0.0f,0.0f,0.0f),glm::vec3(0.7f,0.0f,0.0f),
                glm::vec3(3.0f,0.0f,0.0f),glm::vec3(0.7f,0.0f,0.0f),
                glm::vec3(0.0f,0.0f,0.0f),glm::vec3(0.0f,0.7f,0.0f),
                glm::vec3(0.0f,3.0f,0.0f),glm::vec3(0.0f,0.7f,0.0f),
                glm::vec3(0.0f,0.0f,0.0f),glm::vec3(0.0f,0.0f,0.7f),
                glm::vec3(0.0f,0.0f,3.0f),glm::vec3(0.0f,0.0f,0.7f)};

        // Create buffers/arrays
        glGenVertexArrays(1, &this->VAO);
        glGenBuffers(1, &this->VBO);


        glBindVertexArray(this->VAO);
        // Load data into vertex buffers
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        // A great thing about structs is that their memory layout is sequential for all its items.
        // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
        // again translates to 3/2 floats which translates to a byte array.
        glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(glm::vec3), vertices, GL_STATIC_DRAW);


        // Set the vertex attribute pointers
        // Vertex Positions
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2* sizeof(glm::vec3), (GLvoid*)0);
        // Vertex Normals
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,2* sizeof(glm::vec3), (GLvoid*)(sizeof(glm::vec3)));


        glBindVertexArray(0);
    }

};
#endif //CP16_DRAWAXIS_H
