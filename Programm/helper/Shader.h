//
// Created by Knork on 01.06.2016.
//

#ifndef C1_SHADER_H
#define C1_SHADER_H


#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h> // Include glew to get all the required OpenGL headers

class Shader
{
    std::string readFileToString(const GLchar *path);
public:
    // The program ID
    GLuint Program;
    // Constructor reads and builds the shader
    Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
    // Use the program
    void use();
};


#endif //C1_SHADER_H
