//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_WINDOW_H
#define CP16_WINDOW_H



// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

#include "ASSERT.h"


class Window {



    GLFWwindow *window;

public:
    GLuint screenWidth = 1920, screenHeight = 1080;


    Window() {

        // Init GLFW
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

        const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        screenWidth = mode->width;
        screenHeight = mode->height;

//         window = glfwCreateWindow(screenWidth, screenHeight, "For Giants", nullptr, nullptr); // Windowed
        window = glfwCreateWindow(screenWidth, screenHeight, "My Title", glfwGetPrimaryMonitor(), NULL);
        glfwSetWindowPos(window, 200, 30);

        glfwMakeContextCurrent(window);


//        const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
//        glfwSetWindowMonitor(window, glfwGetPrimaryMonitor(), 0, 0, mode->width, mode->height, mode->refreshRate);


//        // fullScreen
//        // So gehts! Nicht anfassen!!!
//        screenWidth = 2560;
//        screenHeight = 1440;
//        glfwWindowHint(GLFW_DECORATED, 0);
//        //  glfwGetMonitors(&count)[0]
//        GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "ImGui OpenGL3 example",glfwGetPrimaryMonitor() ,NULL);
//        glfwMakeContextCurrent(window);
////        glfwSwapInterval(0);
//        // ----------------------


        // Options
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        // Initialize GLEW to setup the OpenGL Function pointers
        glewExperimental = GL_TRUE;
        glewInit();

        // Define the viewport dimensions
        glViewport(0, 0, screenWidth, screenHeight);

        // Setup some OpenGL options
        glEnable(GL_DEPTH_TEST);

        glfwMakeContextCurrent(window);
//        glfwSwapInterval(1.0);

        ASSERT(window!=NULL ,"Failed to create GLFW window");
        ASSERT(glewInit() == GLEW_OK , "Failed to initialize GLEW");

    }



    GLFWwindow* getWindow(){
            return window;
    }

};


#endif //CP16_WINDOW_H
