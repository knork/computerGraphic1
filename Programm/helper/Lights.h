//
// Created by Knork on 23.06.2016.
//

#ifndef SENG_LIGHT_H
#define SENG_LIGHT_H

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <GL/glew.h>
#include <algorithm>
#include "Shader.h"
#include "Camera.h"


class Lights {

    const GLuint lightBufferSize = 30 * 48+16;

    struct Light {
        glm::vec3 ambient; // 12 Byte
        glm::float32 padding; // 4 Bytes
        // 16
        glm::vec3 pos;// 12 Bytes
        glm::float32 padding2; // 4 Bytes
        // 32
        glm::uint type; // 4 Bytes
        glm::float32 spec; // 4 Bytes
        glm::float32 padding3; // 4 Bytes
        glm::float32 padding4; // 4 Bytes
        // Total : 48 Bytes

    };

    /**
     * Sorts Ligts depending on distance to the Player
     * @return
     */
    bool lightSort() {

    }

    GLuint uboMatrices;
    const GLuint bufferIndex = 1;

    std::vector<Light> allLights;
    std::vector<std::vector<InstanceMesh*>> lightMeshes;
    Shader *shader;




    void init() {
        glGenBuffers(1, &uboMatrices);
        glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
        glBufferData(GL_UNIFORM_BUFFER, lightBufferSize, NULL, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        glBindBufferRange(GL_UNIFORM_BUFFER, bufferIndex, uboMatrices, 0, lightBufferSize * sizeof(GLbyte));
    }



public:

    const static GLuint directional = 0;
    const static GLuint point = 1;

    Lights() {
        shader = new Shader("shader/vLight.glsl","shader/fLight.glsl");
        init();
    }

    // TODO delete Lights somewhere, here would probably be pretty good...
    ~Lights(){
        delete shader;
    }

    void reset(){
        allLights.clear();
        for(auto& v: lightMeshes){
            for(auto *m:v){
                m->reset();
            }
        }
    }

    GLuint addLight(GLuint type,  glm::vec3 ambient, glm::vec3 pos, GLfloat spec) {
        allLights.push_back({ambient, 0.0f,pos, 0.0f,type, spec,0.0f,0.0f});
        return allLights.size() - 1;
    }

    struct less_than_key
    {
        inline bool operator() (const Light& struct1, const Light& struct2)
        {
            float distanceA = glm::distance(game::camera->Position,struct1.pos);
            float distanceB = glm::distance(game::camera->Position,struct2.pos);

            if(distanceA < -1.0f && distanceB >0.0f)
                return false;
            if(distanceB < -1.0f && distanceA >0.0f)
                return false;

            return fabs(distanceA)< fabs(distanceB);
        }
    };


    /**
     * Updates the Light UBO
     */

    void update() {


        glm::uint amount = allLights.size();
        std::vector<Light> copy = allLights;
        std::vector<Light> curLights;


        // sort list dependand on distance to the player



        std::sort(copy.begin(),copy.end(),less_than_key());

//        int what =0;
        for(int i =0;i<amount || i <30;i++){
            curLights.push_back(copy[i]);
        }


        if(amount>=30)
            amount =30;

        glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);

        // offset is 16 ... why?!? dunno

        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::uint), &(amount));

        glBufferSubData(GL_UNIFORM_BUFFER, 16, amount * sizeof(Light), &curLights[0]);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }
    /**
     * We wish to use a separate Shader programm for the displaying of lights
     */
    void addMeshes(std::vector<InstanceMesh*> meshes){
        lightMeshes.push_back(meshes);
    }

    void draw(){
        shader->use();

        for(auto& v: lightMeshes){
            for(auto *m:v){
                m->draw(shader);
            }
        }
    }

    Light& getLight(GLuint index){

        return allLights[index];
    }




};



#endif //SENG_LIGHT_H
