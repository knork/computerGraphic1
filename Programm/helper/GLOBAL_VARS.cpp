//
// Created by Knork on 17.07.2016.
//

#include "GLOBAL_VARS.h"

GLfloat game::deltaTime=0.0f;
//GLfloat game::screenHeight,game::screenWidth;

StateManager* game::stageManager = NULL;

ObstacleLookUp *game::obstacleLookup = NULL;

StringRenderer *game::stringRenderer = NULL;

TunnelGeneration *game::tunnelGeneration = NULL;

GameState *game::gameState = NULL;

Camera *game::camera = NULL;