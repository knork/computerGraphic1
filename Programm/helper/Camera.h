//
// Created by Knork on 02.06.2016.
// Copyright notice:
// ~40-60% copied form learnOpenGL
// File: /CopyrightMaterial/Camera.h
//

#ifndef C1_CAMERA_H
#define C1_CAMERA_H
#include <iostream>
#include<GL/glew.h>
#include <GLFW/glfw3.h>
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/constants.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "MovmentTypes.h"
#include "GLOBAL_VARS.h"


// Default camera values
const GLfloat YAW        = -90.0f;
const GLfloat PITCH      =  0.0f;
const GLfloat SPEED      =  15.0f;
const GLfloat SENSITIVTY =  0.25f;
const GLfloat ZOOM       =  glm::radians(45.0f);
const GLint FOVSENS      =  50;

/**
 * Yaw of 90 => looking down the z-axis
 * Yaw of 0 => looking down the x-axis
 *
 */

class Camera {
    GLint fovSteps;
    GLuint uboMatrices;

    void setUpBuffers(){
        glGenBuffers(1, &uboMatrices);

        glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
        // 196 Bytes
        glBufferData(GL_UNIFORM_BUFFER, 208, NULL, GL_STATIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        glBindBufferRange(GL_UNIFORM_BUFFER, 0, uboMatrices, 0, 208);
    }

    glm::mat4 proj;

public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    glm::vec3 offset;

    // Eular Angles
    GLfloat Yaw;
    GLfloat Pitch;
    // Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;
    GLfloat Zoom;


    // Constructor with vectors
    Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch, GLfloat aspectRatio) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
    {

        game::camera=this;
        this->Position = position;
        this->WorldUp = up;
        this->Yaw = yaw;
        this->Pitch = pitch;
        this->updateCameraVectors();
        offset = glm::vec3(0.0f,3.0f,-10.0f);
        proj = glm::perspective(Zoom, aspectRatio, 0.1f, 100000.0f);
        setUpBuffers();
    }
    // Constructor with scalar values
    Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch, GLfloat aspectRatio) : Camera(
            glm::vec3(posX, posY, posZ), glm::vec3(upX, upZ, upZ), yaw, pitch, aspectRatio)
    {    }

    void updateMatricies(){

        glm::mat4 view = getViewMatrix();
        glm::mat4 projView = proj * view;

        glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(proj));
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
        glBufferSubData(GL_UNIFORM_BUFFER, 2*sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(projView));
        glBufferSubData(GL_UNIFORM_BUFFER, 3*sizeof(glm::mat4), sizeof(glm::vec3), glm::value_ptr(Position));
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    // Returns the view matrix calculated using Eular Angles and the LookAt Matrix
    glm::mat4 getViewMatrix()
    {
        //return glm::lookAt(this->Position, , this->Up);
        return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
    }

    // Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void processKeyboard(MovementDirections direction, GLfloat deltaTime)
    {
        GLfloat velocity = this->MovementSpeed * deltaTime;
        if (direction == FORWARD)
            this->Position += this->Front * velocity;
        if (direction == BACKWARD)
            this->Position -= this->Front * velocity;
        if (direction == LEFT)
            this->Position -= this->Right * velocity;
        if (direction == RIGHT)
            this->Position += this->Right * velocity;
    }

    // Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true)
    {
        xoffset *= this->MouseSensitivity;
        yoffset *= this->MouseSensitivity;

        this->Yaw   += xoffset;
        this->Pitch += yoffset;

        // Make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch)
        {
            if (this->Pitch > 89.0f)
                this->Pitch = 89.0f;
            if (this->Pitch < -89.0f)
                this->Pitch = -89.0f;
        }

        // Update Front, Right and Up Vectors using the updated Eular angles
        this->updateCameraVectors();

    }

    // Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void processMouseScroll(GLfloat yoffset)
    {
        fovSteps+=yoffset;
        if(fovSteps >FOVSENS){
            fovSteps= FOVSENS;
        }
        else if( fovSteps < -FOVSENS){
            fovSteps= -FOVSENS;
        }

        Zoom =  (GLfloat)(fovSteps+FOVSENS)/(((GLfloat)FOVSENS)*2) * glm::pi<GLfloat>();




    }

    GLfloat timeStamp=0.0f;
    glm::vec3 fronts[1000];
    GLuint index=0;

    void setFront(glm::vec3 front){

        if(glfwGetTime()-timeStamp>0.001f){
            fronts[index]=front;
            timeStamp=glfwGetTime();
        }
        glm::vec3 tmp;

        for(int i =0;i<1000;i++){
            tmp+=fronts[i];
        }

        tmp = glm::normalize(tmp);
        front = -glm::normalize(tmp);
        if(glm::length(front)>0.01f) {
            GLfloat yaw = glm::degrees(acosf(front.x));

//            GLfloat diff = Yaw - yaw;
            Yaw = yaw;

            updateCameraVectors();
        }

//        if(glm::length(front)>0.2)
//            lookAt(Position+front);
    }

    void setPos(glm::vec3 pos){
        this->Position = pos+(offset.x*Right+offset.y*Up+offset.z*Front);
    }


    void lookAt(glm::vec3 in){

//        in = in - Position;
//        in = glm::normalize(in);
//        Pitch= glm::degrees(acosf(in.y));
//        Yaw = glm::degrees(acosf(in.z));

        in = in - Position;

        glm::vec3 pitchVec = glm::normalize(glm::vec3(in.x,0.0f,in.z));
        glm::vec3 yawVec = glm::normalize(glm::vec3(in.x,in.y,0.0f));

        Pitch= glm::degrees(cosf(in.y));
        Yaw = glm::degrees(acosf(in.x));

        updateCameraVectors();
    }



    // Calculates the front vector from the Camera's (updated) Eular Angles
    void updateCameraVectors()
    {

        // Calculate the new Front vector
        glm::vec3 front;
        front.x = cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        front.y = sin(glm::radians(this->Pitch));
        front.z = sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        this->Front = glm::normalize(front);
        // Also re-calculate the Right and Up vector
        this->Right = glm::normalize(glm::cross(this->Front, this->WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
        this->Up    = glm::normalize(glm::cross(this->Right, this->Front));
    }


};


#endif //C1_CAMERA_H
