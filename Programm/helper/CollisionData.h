//
// Created by Knork on 03.07.2016.
//
// Idea from
//  http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php
//

#ifndef CP16_COLLISIONPLANE_H
#define CP16_COLLISIONPLANE_H


#include <GL/glew.h>
#include <glm/glm.hpp>


struct CollisionPlane{

    // normalized!
   glm::vec3 normal;
//    GLfloat D;
    // for range checks
    GLfloat xmin,xmax,zmin,zmax;
    glm::mat4 inverseTransform;

};

struct DynCollisionPlane: CollisionPlane{
    DynCollisionPlane(CollisionPlane p){
        normal = p.normal;
        xmin = p.xmin;
        xmax = p.xmax;
        zmin = p.zmin;
        zmax = p.zmax;
        inverseTransform = p.inverseTransform;
        force = 1.0f;
    }
    GLfloat force;
    glm::vec3 direction;
};


struct CollionBox{
    glm::vec3 origin,a,b,c;
};
#endif //CP16_COLLISIONPLANE_H
