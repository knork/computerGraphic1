//
// Created by Knork on 28.06.2016.
// Copied From: Model.h
//

#ifndef C1_MODEL_H
#define C1_MODEL_H


#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


#include <glm/gtx/string_cast.hpp>
#include <assimp/Importer.hpp>
#include "../game/generation/TunnelSegmentParser.h"
#include "MeshStructs.h"
#include "PlayerMesh.h"

//GLint TextureFromFile(const char* path, string directory);

class PlayerModel
{
public:
    /*  Functions   */
    // Constructor, expects a filepath to a 3D model.
    PlayerModel(GLchar* path)
    {
        this->loadModel(path);
    }

    // Draws the model, and thus all its meshes
    void Draw(Shader shader)
    {
        for(GLuint i = 0; i < this->meshes.size(); i++)
            this->meshes[i]->Draw(shader);
    }
    ~PlayerModel(){
        for( PlayerMesh* mesh:meshes){
            delete mesh;
        }
    }



private:
    /*  Model Data  */
    vector<PlayerMesh*> meshes;
    string directory;

    void loadModel(std::string path){

        std::size_t found = path.find_last_of('/');
        this->directory = path.substr(0, found);
        using namespace rapidxml;
        xml_document<> doc;
        xml_node<> *root_node;
        // Read the xml file into a vector
        ifstream theFile(path.c_str());
        /// buffer needs to stay alive during the whole parsing process -> easierst to keep it "unextracted"
        /// not the nicest solution...
        vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
        buffer.push_back('\0');
        // Parse the buffer using the xml file parsing library into doc

        doc.parse<0>(&buffer[0]);

        // Find our root node
        root_node = doc.first_node("TunnelSegment");
        ASSERT(root_node, "XML file missing/wrong path" << path.c_str());



        xml_node<> *cur;

        for (xml_node<> *meshNode = root_node->first_node("mesh"); meshNode; meshNode = meshNode->next_sibling(
                "mesh")) {
            /// Dev note: parse Mesh create a instance and this object will be it's owner!
            /// This object is responsible to delete the mesh


            xml_attribute<>* attr = meshNode->first_attribute("name");

                PlayerMesh *m  = TSParser::parsePlayerMesh(meshNode, directory);
                meshes.push_back(m);


        }



    }




};







#endif //C1_MODEL_H
