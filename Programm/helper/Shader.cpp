//
// Created by Knork on 01.06.2016.
// Largly copied from Shader.h
//

#include "Shader.h"
#include <string>
#include "ASSERT.h"

std::string Shader::readFileToString(const GLchar *path) {

    std::ifstream ifs;

    ifs.open(path,std::ifstream::in);

//    ASSERT(ifs != NULL ,"Failed to find File :"+*path);
    std::string str;

    ifs.seekg(0, std::ios::end);
    str.reserve(ifs.tellg());

    ifs.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(ifs)),
               std::istreambuf_iterator<char>());


    return str;
}

Shader::Shader(const GLchar *vertexPath, const GLchar *fragmentPath) {

    GLuint vertexShader;

    vertexShader = glCreateShader(GL_VERTEX_SHADER);

  //  std::cout << vertexPath << std::endl << readFileToString(vertexPath) << std::endl;


    std::ifstream ifs;

    ifs.open(vertexPath,std::ifstream::in);

//    ASSERT(ifs != NULL , std::string("Failed to find File: ")+(vertexPath));
    std::string str;

    ifs.seekg(0, std::ios::end);
    str.reserve(ifs.tellg());

    ifs.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(ifs)),
               std::istreambuf_iterator<char>());

    const char* vShaderSource = (str.c_str());

    glShaderSource(vertexShader,1,(const GLchar **) &vShaderSource ,NULL);
    glCompileShader(vertexShader);

    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(vertexShader,GL_COMPILE_STATUS, &success);

    if(!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED"<<vertexPath<<"\n" <<
        infoLog << std::endl;
    }



    GLuint fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    std::ifstream ifs2;

    ifs2.open(fragmentPath,std::ifstream::in);

//    ASSERT(ifs2 != NULL , std::string("Failed to find File: ")+(fragmentPath));


    ifs2.seekg(0, std::ios::end);
    str.reserve(ifs2.tellg());

    ifs2.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(ifs2)),
               std::istreambuf_iterator<char>());

    const char* fShaderSource = str.c_str();

    glShaderSource(fragmentShader, 1, (const GLchar **) &fShaderSource, NULL);


    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader,GL_COMPILE_STATUS, &success);

    if(!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::Fragment::COMPILATION_FAILED"<< fragmentPath<<"\n" <<
        infoLog << std::endl;
    }



    GLuint shaderProgram;

    shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::LINKING_FAILED - \n" <<
        infoLog << std::endl;
    }

    glUseProgram(shaderProgram);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    Program = shaderProgram;

    // Alpha set up uniform

    GLuint uniformBufferIndex = glGetUniformBlockIndex(Program,"Matrices");
    glUniformBlockBinding(Program, uniformBufferIndex, 0);

    GLuint uniformBufferIndexLights = glGetUniformBlockIndex(Program,"Lights");
    glUniformBlockBinding(Program, uniformBufferIndexLights, 1);


}

void Shader::use() {
    glUseProgram(this->Program);
}

