//
// Created by Knork on 24.07.2016.
//

#ifndef CP16_MESHSTRUCTS_H
#define CP16_MESHSTRUCTS_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>

struct Vertex {
    // Position
    glm::vec3 Position;
    // Normal
    glm::vec3 Normal;
    // TexCoords
    glm::vec2 TexCoords;

    glm::vec3 Tangent;
};
const std::string TEXTURE_DIFFUSE = "texture_diffuse";
const std::string TEXTURE_SPECULAR = "texture_specular";
struct Texture {
    GLuint id;
    std::string type;
    std::string path;
};


#endif //CP16_MESHSTRUCTS_H
