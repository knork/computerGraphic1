//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_GLOBAL_VARS_H
#define CP16_GLOBAL_VARS_H

#include <GL/glew.h>


class StateManager;
class ObstacleLookUp;
class StringRenderer;
class TunnelGeneration;
class Camera;
class GameState;

/**
 * ALL objects that are represented here only exist once
 * next time i will make them static or don't even bother
 * with classes...
 */
namespace game {

    extern GLfloat deltaTime;
//  extern GLfloat screenHeight,screenWidth;
    extern StateManager *stageManager;
    extern ObstacleLookUp *obstacleLookup;
    extern StringRenderer * stringRenderer;

    extern TunnelGeneration * tunnelGeneration;
    extern Camera *camera;
    extern GameState * gameState;

    }

#endif //CP16_GLOBAL_VARS_H
