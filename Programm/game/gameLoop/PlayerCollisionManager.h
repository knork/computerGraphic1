//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_COLLISIONMANAGER_H
#define CP16_COLLISIONMANAGER_H

//#include "Player.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <chrono>
#include "../../helper/CollisionData.h"
#include "../../helper/MovmentTypes.h"
#include "../../helper/GLOBAL_VARS.h"
#include "../generation/ObstacleLookUp.h"


class PlayerCollisionManager {

    std::vector<CollisionPlane> planes;
    std::vector<CollisionPlane> winPlanes;
    std::vector<std::vector<DynCollisionPlane> *> *dynPlanes;

    ObstacleLookUp * const olu;

    // Atrributes of the Player
    // the player only needs to know it's Position
    // we handle the rest here

    glm::vec3 gravity;
    glm::vec3 force;

    glm::vec3 startPos;
    glm::vec3 position;
    glm::vec3 lastPosition;
    glm::vec3 velocity;

    glm::quat rotation;

    glm::vec3 drag;

    GLfloat movmentSpeed = 7;
    GLfloat bounce = 1.15f;

    // player is a sphere thus we need the radius
    GLfloat radius;

    GLfloat smallestDistanceToPlane;
    GLdouble lastCollision;

    GLdouble winFirstTouch = -1.0f;
    glm::vec3 directionOfMovement;

    GLboolean staticCamera=true;

    /**
     *
     * Check collision with a sphere (the player)
     * Only very simple collision check -  fails to find collision where the
     * center of the sphere is outside the plane but the sphere still collides
     * needs to be improved at some point...
     *
     *
     * @param cp - collision Plane that should be checked for collision
     * @param nextPosBoxCoords - next position of the player(or whatever we want to check) ( in plane Coords!)
     *          so the position the player would be if no collision occurs
     * @param curPos - poisiton the player is curently at (again plane coords)
     * @return true if collision occured
     */
    inline bool collision(CollisionPlane *cp, glm::vec4 nextPosBoxCoords,glm::vec4 curPos){

        // check distance to plane
        GLfloat dist = glm::length(nextPosBoxCoords);

        GLfloat r = fabs(radius * glm::length(cp->inverseTransform * glm::vec4(cp->normal, 0.0f)));

        if(dist < smallestDistanceToPlane){
            smallestDistanceToPlane=dist;
        }
        // linear system
        // see if the y hit's the plane at any point
        GLfloat m = (r -curPos.y) /nextPosBoxCoords.y;

        if(m>1 || m <0){
            return false;
        }

        if(nextPosBoxCoords.z > cp->zmax || nextPosBoxCoords.z < cp->zmin){
            return false;
        }

        if (nextPosBoxCoords.x > cp->xmax || nextPosBoxCoords.x < cp->xmin ) {
            return false;
        }

        lastCollision = glfwGetTime();

        return true;
    }


/// TODO see if a inbetween collision happened (very fast movement/ very slow Pc)

    /**
     * Does all the static collision handeling      *
     */
    void staticCollision() {
        CollisionPlane *cp;
        /// what will be added to position for the next frame
        glm::vec3 additionalForce = (force + (-(gravity * gravity))) * game::deltaTime;

        /// next Position the object will move to
        glm::vec3 nextPos = position + velocity * game::deltaTime + additionalForce;


        for (size_t i = 0; i < planes.size(); i++) {
            cp = &planes[i];
            // order
            glm::vec4 tmp = (glm::vec4(nextPos, 1.0f));

            glm::vec4 nextPosBoxCoords = (cp->inverseTransform) * tmp;
            glm::vec4 curPosBoxCoords = (cp->inverseTransform) * glm::vec4(position,1.0f);

            if(!collision(cp,nextPosBoxCoords,curPosBoxCoords)){
                continue;
            }
            /// okay so we do have a collision! let's handle it

            glm::vec3 add = velocity + additionalForce;
            GLfloat dot = glm::max(glm::dot(-add, cp->normal), 0.0f);
            /// project the -velocity onto the normal vektor the wall "pushes" the wall so it doesn't fall through
            glm::vec3 negImpact = dot * cp->normal;
            /// small bounce factor, everybody loves to bounce
            velocity += bounce * negImpact;
            additionalForce = glm::vec3(0.0f);
        }
        position += velocity * game::deltaTime;
        velocity += additionalForce;
    }


    /**
     *  Does all the dynamic collision (currently only with Pendulums)
     */
    void dynamicCollision() {


        DynCollisionPlane *cp;
        /// what will be added to position for the next frame
        glm::vec3 additionalForce = (force + (-(gravity * gravity))) * game::deltaTime;
        /// next Position the object will move to
        glm::vec3 nextPos = position + velocity * game::deltaTime + additionalForce;/// world-coords

        for (size_t i = 0; i < dynPlanes->size(); i++) {
            for (size_t k = 0; k < dynPlanes->at(i)->size(); k++) {
                cp = &(dynPlanes->at(i)->at(k));
                // order
                glm::vec4 tmp = (glm::vec4(nextPos, 1.0f));


                glm::vec4 nextPosBoxCoords = (cp->inverseTransform) * tmp;
                glm::vec4 curPosBoxCoords = (cp->inverseTransform) * glm::vec4(position,1.0f);

                GLfloat r = fabs(radius * glm::length(cp->inverseTransform * glm::vec4(cp->normal, 0.0f)));

                if(!collision(cp,nextPosBoxCoords,curPosBoxCoords)){
                    continue;
                }

                /// okay so we probably have a collision... mostlikly missed a bunch but...

                glm::vec3 add = velocity + additionalForce;
                GLfloat dot = glm::max(glm::dot(-add, cp->normal), 0.0f);
                /// project the -velocity onto the normal vektor
                glm::vec3 negImpact = (dot * cp->normal)*bounce; /// force that keeps the player from driving into the plane
                /// dynamic collision also adds force of it's own
                /// force value depends on the movement vs normal of the plane
                /// => left->right swing will apply 0 force to front collision
                negImpact += cp->direction * cp->force;
                velocity += bounce * negImpact;
                additionalForce += glm::vec3(0.0f);
            }
        }

        position += velocity * game::deltaTime;
        velocity += additionalForce;
        velocity = velocity - (velocity * drag) * game::deltaTime;

    }


public:

    PlayerCollisionManager(GLfloat radius, ObstacleLookUp *olu):
            olu(olu){
        position = glm::vec3(0.0f, 3.0f, 0.0f);
        lastPosition = position;
        startPos=position;
        gravity = glm::vec3(0.0f, 2.5f, 0.0f);
        velocity = glm::vec3(0.0f, 0.0f, 0.0f);
        force = glm::vec3(0.0f, 0.0f, 0.0f);
        drag = glm::vec3(0.1f, 0.1f, 0.1f);
        this->dynPlanes = olu->getAllPlanes();
        this->radius = radius;
    }

    void addForce(glm::vec3 add) {

    }

    void add(CollisionPlane cp) {
        planes.push_back(cp);
    }

    void setWinCollision(std::vector<CollisionPlane> input){
        winPlanes=input;
    }

    void doCollsion() {
        smallestDistanceToPlane=10000.0f;/// large value to start with
        staticCollision();
        olu->animateAll();
        dynamicCollision();
        winCollision();
        checkRepawn();
    }

    /**
     * checks depending on distance to segments, height and last
     * time a collision occured weather the player needs to be respwaned
     */
    void checkRepawn();

    void respawn(){
        position=startPos;
        lastPosition=position;
        velocity= glm::vec3(0.0f);
        rotation=glm::quat();
        winFirstTouch= -1.0f;
    }

    GLfloat curAngle=0,angleChange;


    void winCollision();

    /**
     *
     * Rotates the player model depending on distance traveld
     *
     * @return mat4 that rotates the player model
     */
    glm::mat4 getModel() {
        glm::mat4 out;
        directionOfMovement = lastPosition - position ;
        if(glm::length(directionOfMovement)>0.0001f) {

            /// on Ground -> rotation depends on movement
            if( (glfwGetTime() - lastCollision)<0.2) {


                glm::vec3 rotationAxis = glm::cross(directionOfMovement, glm::vec3(0.0f, 1.0f, 0.0f));
                GLfloat angle = glm::length(directionOfMovement) / radius;

                if (glm::length(rotationAxis) > 0.0000001f) {

                    rotationAxis = glm::normalize(rotationAxis);
                    glm::quat curRot = glm::angleAxis(angle, rotationAxis);


                    rotation = curRot * rotation;
                    lastPosition = position;
                }
                angleChange = angle;
                this->curAngle+=angle;


            }else{ /// mid air -> slowly reduce spin
                glm::vec3 rotationAxis = glm::cross(directionOfMovement, glm::vec3(0.0f, 1.0f, 0.0f));
                if (glm::length(rotationAxis) > 0.0000001f) {
                    if (angleChange > 0.0f) {

                        rotationAxis = glm::normalize(rotationAxis);
                        angleChange = angleChange - (angleChange * 0.9f) * game::deltaTime;

                        glm::quat curRot = glm::angleAxis(angleChange, rotationAxis);
                        rotation = curRot * rotation;
                    }
                }

            }

        }
        out = glm::translate(out, position);
        out = out*glm::mat4_cast(rotation);

        return out;
    }

    glm::vec3 getPosition() {
        return position;
    }


    void processKeyboard(MovementDirections direction) {

        /**
         * Movement depending on direction that player looks into
         * only used for crappy F2 (alternate) camera
         */
        if (direction == FORWARD)
            this->velocity += movmentSpeed * game::camera->Front * game::deltaTime;
        if (direction == BACKWARD)
            this->velocity += movmentSpeed * -game::camera->Front * game::deltaTime;
        if (direction == LEFT)
            this->velocity += movmentSpeed * -game::camera->Right * game::deltaTime;
        if (direction == RIGHT)
            this->velocity += movmentSpeed * game::camera->Right * game::deltaTime;

//        if (direction == FORWARD)
//            this->velocity += movmentSpeed * glm::vec3(0.0f, 0.0f, 1.0f) * game::deltaTime;
//        if (direction == BACKWARD)
//            this->velocity += movmentSpeed * glm::vec3(0.0f, 0.0f, -1.0f) * game::deltaTime;
//        if (direction == LEFT)
//            this->velocity += movmentSpeed * glm::vec3(1.0f, 0.0f, 0.0f) * game::deltaTime;
//        if (direction == RIGHT)
//            this->velocity += movmentSpeed * glm::vec3(-1.0f, 0.0f, 0.0f) * game::deltaTime;
    }

    void reset(){
        respawn();
        planes.clear();
    }


    glm::vec3 getDir(){
        return directionOfMovement;
    }

    void toogleCameraMode();
    bool getCamMode(){
        return staticCamera;
    }
};

#endif //CP16_COLLISIONMANAGER_H
