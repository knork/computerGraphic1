//
// Created by Knork on 25.07.2016.
//

#include "InputHandler.h"
#include "../states/GameState.h"

const GLuint InputHandler::cameraState = 0;
const GLuint InputHandler::gameState = 1;

GLuint InputHandler::currentState = 1;

Camera *InputHandler::camera;
PlayerCollisionManager *InputHandler::col;
Window *InputHandler::window;


bool InputHandler::keys[1024];

void InputHandler::InputHandler(PlayerCollisionManager *in, Window *win, Camera *cam) {
    ASSERT(in != NULL, "CollisionManager is NULL");
    ASSERT(win != NULL, "window is NULL");
    camera = cam;
    window = win;
    col = in;


}

void InputHandler::key_callback(GLFWwindow *window, int key, int scancode, int action, int mode) {
    ASSERT(window != NULL, "window pointer is null");

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if (key == GLFW_KEY_F1 && action == GLFW_PRESS) {
        if (currentState == cameraState) {
            camera->Yaw = 90;
            camera->updateCameraVectors();
            currentState = gameState;
        }
        else if (currentState == gameState) {
            currentState = cameraState;
        }
    }
    if ((key == GLFW_KEY_F2 && action == GLFW_PRESS)) {
        col->toogleCameraMode();
    }

    ASSERT(game::gameState!=NULL,"gameState isn't set yet");
    if ((key == GLFW_KEY_F4 && action == GLFW_PRESS)) {
        game::gameState->changeGamma(false);
    }
    if ((key == GLFW_KEY_F5 && action == GLFW_PRESS)) {
        game::gameState->changeGamma(true);
    }

    if (action == GLFW_PRESS)
        keys[key] = true;
    else if (action == GLFW_RELEASE)
        keys[key] = false;
}

void InputHandler::processInputs() {

    if (currentState == cameraState) {
        doCameraMovemenmt();
    }
    if (currentState == gameState) {
        doPlayerMovement();
    }


}

void InputHandler::doCameraMovemenmt() {
    // Camera controls

    ASSERT(camera != NULL, "Camera pointer is null. Forgot to call init func InputHandler(...) ?");

    if (keys[GLFW_KEY_W]) {
        camera->processKeyboard(FORWARD, game::deltaTime);


    }
    if (keys[GLFW_KEY_S]) {
        camera->processKeyboard(BACKWARD, game::deltaTime);

    }
    if (keys[GLFW_KEY_A]) {
        camera->processKeyboard(LEFT, game::deltaTime);
    }
    if (keys[GLFW_KEY_D]) {
        camera->processKeyboard(RIGHT, game::deltaTime);
    }
}

void InputHandler::doPlayerMovement() {

    ASSERT(col != NULL, "PlayerCollisionManager pointer is null. Forgot to call init func InputHandler(...) ?");
    // Camera controls
    if (keys[GLFW_KEY_W]) {
        col->processKeyboard(FORWARD);
    }
    if (keys[GLFW_KEY_S]) {
        col->processKeyboard(BACKWARD);

    }
    if (keys[GLFW_KEY_A]) {
        col->processKeyboard(LEFT);
    }
    if (keys[GLFW_KEY_D]) {
        col->processKeyboard(RIGHT);
    }


    camera->setPos(col->getPosition());
    if (!col->getCamMode()) {
        camera->setFront(col->getDir());
    }

}

bool InputHandler::firstMouse = true;
GLfloat InputHandler::lastX, InputHandler::lastY;

void InputHandler::mouse_callback(GLFWwindow *window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    if (currentState != gameState)
        camera->processMouseMovement(xoffset, yoffset);
}

void InputHandler::setActive() {
    ASSERT(InputHandler::window != NULL, "Forgot to call InputHandler::InputHandler first...");
    glfwSetKeyCallback(InputHandler::window->getWindow(), InputHandler::key_callback);
    glfwSetCursorPosCallback(InputHandler::window->getWindow(), InputHandler::mouse_callback);
    for (int i = 0; i < 1024; i++) {
        keys[i] = false;
    }


}
