#include "PlayerCollisionManager.h"
#include "../StateManager.h"

void PlayerCollisionManager::winCollision() {
    CollisionPlane *cp;
    // what will be added to position for the next frame
    glm::vec3 additionalForce = (force + (-(gravity * gravity))) * game::deltaTime;

    // next Position the object will move to
    glm::vec3 nextPos = position + velocity * game::deltaTime + additionalForce;


    for (size_t i = 0; i < winPlanes.size(); i++) {
        cp = &winPlanes[i];
        // order
        glm::vec4 tmp = (glm::vec4(nextPos, 1.0f));
        glm::vec4 nextPosBoxCoords = (cp->inverseTransform) * tmp;
        glm::vec4 curPosBoxCoords = (cp->inverseTransform) * glm::vec4(position, 1.0f);
        if (!collision(cp, nextPosBoxCoords, curPosBoxCoords)) {
            continue;
        }
        // okay so we do have a collision!

        glm::vec3 add = velocity + additionalForce;

        GLfloat dot = glm::max(glm::dot(-add, cp->normal), 0.0f);
        // project the -velocity onto the normal vektor the wall "pushes" the wall so it doesn't fall through
        glm::vec3 negImpact = dot * cp->normal;
        // small bounce factor

        if (winFirstTouch < 0.0) {
            winFirstTouch = glfwGetTime();
        }
        if (glfwGetTime() - winFirstTouch > 0.15f) {
            /// this call can ONLY be done if we are in a separate .cpp file...
            game::stageManager->setNextState(StateManager::PLAYERWON);

        }


    }




    // std::cout << player->getPosition().y << std::endl;
}

void PlayerCollisionManager::checkRepawn() {

    if(position.y>-50){
        return;
    }

    if (smallestDistanceToPlane > 7 && (glfwGetTime() - lastCollision) > 1.5) {
        // respawn

        if (game::tunnelGeneration->getLives() > 1) {
            game::tunnelGeneration->getLives()--;
            respawn();
        }else{
            game::stageManager->setNextState(StateManager::PLAYERLOST);
        }

    }


}

void PlayerCollisionManager::toogleCameraMode() {
    if(staticCamera){
        staticCamera=false;
    }else{
        game::camera->Yaw=90;
        game::camera->updateCameraVectors();
        staticCamera=true;

    }

}
