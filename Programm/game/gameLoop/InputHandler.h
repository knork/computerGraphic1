//
// Created by Knork on 04.07.2016.
//

#ifndef CP16_INPUTHANDLER_H
#define CP16_INPUTHANDLER_H


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "../../helper/MovmentTypes.h"
#include "../../helper/Window.h"
#include "../../helper/ASSERT.h"
#include "../../helper/Camera.h"
#include "PlayerCollisionManager.h"


/**
 * Handels the input for the GAME.. not the menus though
 */
namespace InputHandler{

    /**
     * A,S,D,W movement
     * F1 debug cam
     * F2 alternate cam toggle
     * F4 reduce gamma
     * F5 increase gamma
     */


    extern const GLuint cameraState;
    extern const GLuint gameState ;

    extern GLuint currentState;

    extern Camera *camera;
    extern PlayerCollisionManager* col;
    extern Window* window;

    extern bool firstMouse;
    extern GLfloat lastX,lastY;


    extern bool keys[1024];

    void InputHandler(PlayerCollisionManager * in,Window* win,Camera * cam);


    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

    /// only used for debug cam
    void mouse_callback(GLFWwindow* window, double xpos, double ypos);


    void processInputs();

    void doCameraMovemenmt();

    void doPlayerMovement();

    void setActive();
};
// otherwise the callback won't work!
//bool InputHandler::keys[1024];
#endif //CP16_INPUTHANDLER_H
