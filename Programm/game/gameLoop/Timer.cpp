//
// Created by Knork on 19.08.2016.
//

#include <glm/glm.hpp>
#include "Timer.h"
#include "../../helper/GLOBAL_VARS.h"

Timer::Timer() {
    stringRenderer = game::stringRenderer;
    ASSERT(stringRenderer!=NULL,"string Renderer was not initialized!");

}

void Timer::reset() {
    start = std::chrono::system_clock::now();
}

std::string Timer::getCurrent() {

    int milliseconds =  std::chrono::duration_cast<std::chrono::milliseconds>( end-start).count();

    int minutes = milliseconds /60000;
    milliseconds = milliseconds%60000;

    int seconds = milliseconds /1000;
    milliseconds = milliseconds%1000;

//    std::string string = ""+std::to_string(minutes)+" : "+std::to_string(seconds)+" : "+std::to_string(milliseconds);


    char buff[100];
    snprintf(buff, sizeof(buff), "%02i:%02i:%02i", minutes,seconds,milliseconds/10);
    std::string string = buff;

    return string;
}

void Timer::draw() {
    end = std::chrono::system_clock::now();







    stringRenderer->RenderTextEvenSpacing(getCurrent(),0.0,0.04,1.6,glm::vec4(0.43,0.0,0.0,0.9));



}
