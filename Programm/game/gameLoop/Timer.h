//
// Created by Knork on 19.08.2016.
//

#ifndef CP16_TIMER_H
#define CP16_TIMER_H


#include "../menu/StringRenderer.h"
#include <chrono>
class Timer {

    StringRenderer * stringRenderer;
    std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> end;

public:
    Timer();
    void reset();
    void draw();
    std::string getCurrent();
};


#endif //CP16_TIMER_H
