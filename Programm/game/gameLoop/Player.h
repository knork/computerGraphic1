//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_PLAYER_H
#define CP16_PLAYER_H


class Shader;
class Camera;
class PlayerCollisionManager;


#include <glm/gtc/type_ptr.hpp>
#include "../../helper/ASSERT.h"
#include "../../helper/PlayerModel.h"
#include "../generation/TunnelSegment.h"
#include "../generation/TunnelGeneration.h"


class Player{

    PlayerModel * playerModel;
    Shader *shader;
    Camera * const camera;

    TunnelSegment *playModel;

    PlayerCollisionManager* const collision;



    glm::vec3 right;
    glm::vec3 front;


public:
    Player(GLchar* path, Camera* camera,PlayerCollisionManager* coli):
            camera(camera),
            collision(coli){
        playerModel = new PlayerModel(path);




        shader = new Shader("shader/vPlayer.glsl","shader/fInstanceShader.glsl");
//        shader = game::tunnelGeneration->instanceShader;
//       shader = new Shader("shader/vinstanceShader.glsl","shader/finstanceShader.glsl");




    }
    ~Player(){
        delete shader;
        delete playerModel;
    }

    void draw(){
        ASSERT(shader,"shader is null");
        ASSERT(playerModel,"PlayerModel is null");
        ASSERT(camera,"camera is null");
        ASSERT(collision,"collision is null");



        glm::mat4 model = collision->getModel();

        shader->use();
        glUniformMatrix4fv(glGetUniformLocation(shader->Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
        playerModel->Draw(*shader);
    }




    GLfloat getRadius(){
        return 1.2;
    }



};
#endif //CP16_PLAYER_H
