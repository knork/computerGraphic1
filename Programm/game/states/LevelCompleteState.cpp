//
// Created by Knork on 07.08.2016.
//

#include <thread>
#include "LevelCompleteState.h"
#include "../../helper/GLOBAL_VARS.h"
#include "../StateManager.h"
#include "HighscoreInputHandler.h"


LevelCompleteState::LevelCompleteState(Window *win):window(win) {
    re = new Panel("model/textures/menu/ButtonRetry.png",glm::vec3(0.25f,0.3f,-3.0f),0.20f,(16.0f)/(9.0f),window);
    left=re;

    next = new Panel("model/textures/menu/ButtonNext.png",glm::vec3(0.55f,0.3f,-3.0f),0.20f,(16.0f)/(9.0f),window);
    right=next;

    Panel *cur = new Panel("model/textures/menu/Victory.png",glm::vec3(0.25f,0.9f,-3.0f),0.40f,(1889.0f)/(480.0f),window);
    allPanels.push_back(cur);

    menu = new Panel("model/textures/menu/ButtonMenu.png",glm::vec3(0.25f,0.3f,-3.0f),0.20f,(16.0f)/(9.0f),window);
    menu->setActive(false);
    restart =new Panel("model/textures/menu/ButtonRestart.png",glm::vec3(0.55f,0.3f,-3.0f),0.20f,(16.0f)/(9.0f),window);
    restart->setActive(true);


//    Panel *bg = new Panel("model/textures/menu/bg_1.png",glm::vec3(0.0f,0.7f,-10.0f),1.00f,(1920.0f)/(400.0f),window);
//    allPanels.push_back(bg);

    HighScoreInput::HighScoreInput(window,this);

    unlitShader = new Shader("shader/vunlitTexture.glsl","shader/funlitTexture.glsl");
    proj = glm::ortho(0.0f,(float)(window->screenWidth),0.0f,(float)window->screenHeight,-1000.0f,1000.0f);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

LevelCompleteState::~LevelCompleteState() {
    for(int i=0;i<allPanels.size();i++){
        delete allPanels[i];
    }
    delete re;
    delete next;
    delete menu;
    delete restart;
}

void LevelCompleteState::handleInput(GLuint key) {

    if(key == LEFT){

        if(curActive==nextID){
            right->setActive(false);
            left->setActive(true);
            curActive=reID;
        }else{
            right->setActive(true);
            left->setActive(false);
            curActive=nextID;
        }

    }
    else if(key == RIGHT){

        if(curActive==nextID){
            right->setActive(false);
            left->setActive(true);
            curActive=reID;
        }else{
            right->setActive(true);
            left->setActive(false);
            curActive=nextID;
        }



    }
    else if(key == ENTER){
        ASSERT(game::stageManager !=NULL , " state Manager in global file is NULL");

        if(!gameOver) {
            if (curActive == nextID) {
                game::stageManager->setNextState(StateManager::NEXT);
            }
            else if (curActive == reID) {
                game::stageManager->setNextState(StateManager::RETRY);
            }
        }
        else{

            if (curActive == nextID) {
                game::stageManager->setNextState(StateManager::RESTART);
            }
            else if (curActive == reID) {
                game::stageManager->setNextState(StateManager::MENU);
            }

        }
    }


}

void LevelCompleteState::setActive() {
    curActive=1;
    next->setActive(true);
    re->setActive(false);

    HighScoreInput::setActive();

    diff = game::tunnelGeneration->getDifficulty();

    amount =  game::tunnelGeneration->getCurLevel();
    time = game::tunnelGeneration->getTimer()->getCurrent();



}

void LevelCompleteState::setGameOver(bool in){
    gameOver=in;
    if(gameOver){
        left=menu;
        right=restart;

    }else{
        left=re;
        right=next;
    }



}

void LevelCompleteState::setDeactive() {

}


void LevelCompleteState::update() {


    if(!gameOver){
        onDefault();
    }else{
        onGameOver();
    }

}


void LevelCompleteState::onGameOver() {


    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    unlitShader->use();
    glUniformMatrix4fv(glGetUniformLocation(unlitShader->Program,"proj"),1,GL_FALSE,&proj[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(unlitShader->Program,"view"),1,GL_FALSE,&view[0][0]);

    for(auto & p:allPanels){
        p->draw(unlitShader);
    }
    menu->draw(unlitShader);
    restart->draw(unlitShader);

    StringRenderer *stringRenderer = game::stringRenderer;

    char buff[100];
    snprintf(buff, sizeof(buff), "Stages cleared %10i", amount-1);
    std::string string = buff;

    stringRenderer->RenderText(string,0.3,0.4,1.2,glm::vec4(0.3,0.0,0.0,1.0));

    ASSERT(game::tunnelGeneration," TunnelGEn pointer NULL");
    ASSERT(game::tunnelGeneration->getScore()," TunnelGEn->score pointer NULL");


    snprintf(buff, sizeof(buff), "Highscore %26i", game::tunnelGeneration->getScore()->getHighScore());
    string = buff;
    stringRenderer->RenderText(string,0.3,0.5,1.2,glm::vec4(0.3,0.0,0.0,1.0));

    snprintf(buff, sizeof(buff), "your Score %22i", game::tunnelGeneration->getScore()->getCount());
    string = buff;
    stringRenderer->RenderText(string,0.3,0.6,1.2,glm::vec4(0.3,0.0,0.0,1.0));

    std::this_thread::__sleep_for(std::chrono::seconds(0),std::chrono::nanoseconds(5000000));

    glfwPollEvents();
    glfwSwapBuffers(window->getWindow());

    game::tunnelGeneration->getScore()->writeScore();

}

void LevelCompleteState::onDefault() {

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    unlitShader->use();
    glUniformMatrix4fv(glGetUniformLocation(unlitShader->Program,"proj"),1,GL_FALSE,&proj[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(unlitShader->Program,"view"),1,GL_FALSE,&view[0][0]);

    for(auto & p:allPanels){
        p->draw(unlitShader);
    }
    re->draw(unlitShader);
    next->draw(unlitShader);

    StringRenderer *stringRenderer = game::stringRenderer;

    char buff[100];
    snprintf(buff, sizeof(buff), "Difficulty %24.2f", diff);
    std::string string = buff;

    stringRenderer->RenderText(string,0.33,0.4,1.2,glm::vec4(0.3,0.0,0.0,1.0));


    snprintf(buff, sizeof(buff), "Stage %38i", amount);
    string = buff;
    stringRenderer->RenderText(string,0.33,0.5,1.2,glm::vec4(0.3,0.0,0.0,1.0));

    snprintf(buff, sizeof(buff), "Score %37i", game::tunnelGeneration->getScore()->getCount());
    string = buff;
    stringRenderer->RenderText(string,0.33,0.6,1.2,glm::vec4(0.3,0.0,0.0,1.0));

    std::this_thread::__sleep_for(std::chrono::seconds(0),std::chrono::nanoseconds(5000000));

    glfwPollEvents();
    glfwSwapBuffers(window->getWindow());

}
