//
// Created by Knork on 07.08.2016.
// Copyright remark 20-30%:http://learnopengl.com/#!Advanced-Lighting/HDR
// HDR.cpp 
//
//

#include "GameState.h"
#include "../gameLoop/InputHandler.h"
#include "../StateManager.h"

GameState::GameState(Window *win)
//            window(win),
//            camera(new Camera(glm::vec3(0.0f, 8.0f, 0.0f),glm::vec3(0.0,1.0,0.0), 0, 0, (float)window->screenWidth/ (float)window->screenHeight)),
//            obstacleLookUp(new ObstacleLookUp()),
//            playerCollision(new PlayerCollisionManager(1.2f,obstacleLookUp)),
//            player(new Player("model/player/Player.obj",camera,playerCollision)),
//            tunnelGen(new TunnelGeneration(playerCollision,obstacleLookUp)),
//            drawAxis(new DrawAxis())
{

    window = (win);
    camera = (new Camera(glm::vec3(0.0f, 8.0f, 0.0f), glm::vec3(0.0, 1.0, 0.0), 0, 0,
                         (float) window->screenWidth / (float) window->screenHeight));
    obstacleLookUp = (new ObstacleLookUp());
    camera->lookAt(glm::vec3(0.0f, 0.0f, 1.0f));
    playerCollision = (new PlayerCollisionManager(1.0f, obstacleLookUp));
    InputHandler::InputHandler(playerCollision, window, camera);




    drawAxis = (new DrawAxis());
    tunnelGen = (new TunnelGeneration(playerCollision, obstacleLookUp));
    player = (new Player("model/player/player.xml", camera, playerCollision));


    // Set up HDR buffers



    hdrFBO;
    glGenFramebuffers(1, &hdrFBO);
    // - Create floating point color buffer
    colorBuffer;
    glGenTextures(1, &colorBuffer);
    glBindTexture(GL_TEXTURE_2D, colorBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, window->screenWidth, window->screenHeight, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // - Create depth buffer (renderbuffer)
    rboDepth;
    glGenRenderbuffers(1, &rboDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,  window->screenWidth, window->screenHeight);
    // - Attach buffers
    glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorBuffer, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    hdrShader = new Shader("shader/vHDR.glsl","shader/fHDR.glsl");

    game::gameState=this;
    // Set up the input Handler
}

GameState::~GameState() {
    delete tunnelGen;
    delete player;
    delete playerCollision;
    delete obstacleLookUp;
    delete camera;
    delete hdrShader;
    // free GPU buffers...
}

void GameState::setActive() {
    time = glfwGetTime();
    InputHandler::setActive();
}

void GameState::setDeactive() {

}


void GameState::update() {
    /// delta Time is handeled an instance above (StageManager)

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glfwPollEvents();

    // update stuff
    camera->updateMatricies();
    InputHandler::processInputs();
    playerCollision->doCollsion();

    // draw into extra buffer
    glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        tunnelGen->draw();
        player->draw();
        drawAxis->draw();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // now we send THAT buffer to the HDR shader YAY, that's so cool xD
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    hdrShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, colorBuffer);
    glUniform1i(glGetUniformLocation(hdrShader->Program, "hdr"), hdr);
    glUniform1f(glGetUniformLocation(hdrShader->Program, "exposure"), exposure);
    glUniform1f(glGetUniformLocation(hdrShader->Program, "gamma"), gamma);
    renderQuad();


    glfwSwapBuffers(window->getWindow());
    time = glfwGetTime();
}

void GameState::renderQuad()
{
    if (quadVAO == 0)
    {
        GLfloat quadVertices[] = {
                // Positions        // Texture Coords
                -1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
                1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
                1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        // Setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

void GameState::retry() {
    playerCollision->respawn();
    tunnelGen->retry();
    game::stageManager->setNextState(StateManager::GAME);
}


void GameState::next() {
    tunnelGen->reset();

    // increase difficulty

    GLfloat dif = tunnelGen->getDifficulty();
    dif = dif + (7 / (7 + dif));
    tunnelGen->makeTunnel(dif);
}

void GameState::restart() {
    tunnelGen->restart();
    tunnelGen->reset();
    tunnelGen->makeTunnel(0.0f);

}

void GameState::changeGamma(bool up) {
    if(up)
        gamma+=0.05;
    else
        gamma-=0.05;
}
