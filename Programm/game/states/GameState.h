//
// Created by Knork on 07.08.2016.
//



#ifndef CP16_GAMESTATE_H
#define CP16_GAMESTATE_H


#include "State.h"
#include "../../helper/Window.h"
#include "../gameLoop/PlayerCollisionManager.h"
#include "../../helper/Camera.h"
#include "../generation/ObstacleLookUp.h"
#include "../gameLoop/Player.h"
#include "../generation/TunnelGeneration.h"
#include "../../helper/DrawAxis.h"

class GameState: public State {

    Window *  window;
    Camera *  camera;
    ObstacleLookUp *  obstacleLookUp;
    PlayerCollisionManager *  playerCollision;
    Player *  player;
    TunnelGeneration *  tunnelGen;
    DrawAxis *  drawAxis;

    GLfloat time;
    GLuint hdrFBO, rboDepth, colorBuffer,quadVAO=0,quadVBO;
    Shader * hdrShader;

    GLboolean hdr = true; // Change with 'Space'
    GLfloat exposure = 3.6f; // Change with Q and E
    GLfloat gamma = 0.9f; // Change with Q and E

    void renderQuad();

public:
    GameState(Window* win);
    ~GameState();

    void setActive() override;
    void setDeactive() override;
    void update() override ;

    void retry();
    void restart();
    void next();

    void changeGamma(bool up);

};


#endif //CP16_GAMESTATE_H
