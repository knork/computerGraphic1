//
// Created by Knork on 07.08.2016.
//

#include "HighscoreInputHandler.h"
#include "LevelCompleteState.h"

Window *HighScoreInput::window;
LevelCompleteState *HighScoreInput::highScore;



void HighScoreInput::HighScoreInput(Window *win, LevelCompleteState *highScore){
    ASSERT(win!= NULL,"Window in HighscoreInputHandler is null...");
    HighScoreInput::window=win;
    HighScoreInput::highScore=highScore;
}

void HighScoreInput::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode) {

    ASSERT(highScore!=NULL,"Forgot to call HighscoreInputHandler::HighscoreInputHandler(...) ");

    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(key == GLFW_KEY_LEFT && action == GLFW_PRESS){
        highScore->handleInput(LevelCompleteState::LEFT);
    }

    if(key == GLFW_KEY_RIGHT && action == GLFW_PRESS){
        highScore->handleInput(LevelCompleteState::RIGHT);
    }
    if(key == GLFW_KEY_ENTER && action == GLFW_PRESS){
        highScore->handleInput(LevelCompleteState::ENTER);
    }

}

void HighScoreInput::setActive() {
    ASSERT(HighScoreInput::window!= NULL,"Forgot to call MenuInput::HighScoreInput(...) ");
    glfwSetKeyCallback(HighScoreInput::window->getWindow(),HighScoreInput::keyCallback);
}

