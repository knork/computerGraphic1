//
// Created by Knork on 07.08.2016.
//

#ifndef CP16_MENUINPUTHANDLER_H
#define CP16_MENUINPUTHANDLER_H

#include "../../helper/Window.h"

#include "LevelCompleteState.h"


namespace HighScoreInput{

    extern Window *window;
    extern LevelCompleteState *highScore;

    

    void HighScoreInput(Window *win, LevelCompleteState *highScore);
    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
    void setActive();

}

#endif //CP16_MENUINPUTHANDLER_H
