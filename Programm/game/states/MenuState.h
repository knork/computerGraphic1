//
// Created by Knork on 07.08.2016.
//

#ifndef CP16_MENUSTATE_H
#define CP16_MENUSTATE_H

#include <glm/glm.hpp>

#include "State.h"
#include<vector>
#include "../../helper/Window.h"
#include "../menu/Panel.h"
#include "../menu/StringRenderer.h"

class MenuState:public State {

    Panel * background;
    Panel * play;
    Panel * exit;
    Shader *unlitShader;

    const GLuint playID=0, exitID =1;

    GLuint curActive=0;
    std::vector<Panel*> allPanels;

    glm::mat4 proj,view;

    Window * const window;
public:

    const static GLuint UP =0;
    const static GLuint DOWN =1;
    const static GLuint ENTER =2;

    MenuState(Window *window);
    ~MenuState();

    virtual void setActive() override;

    virtual void setDeactive() override;

    virtual void update() override;

    void handleInput(GLuint key);
};


#endif //CP16_MENUSTATE_H
