//
// Created by Knork on 07.08.2016.
//

#ifndef CP16_STATE_H
#define CP16_STATE_H
class State{

public:
    /**
     *  Sets the State active.
     *  This ibclude re-register the Inputs
     *
     */
    virtual void setActive() =0;
    /**
     *  Sets the state to deactive.
     *  Free resources other states might need
     */
    virtual void setDeactive()=0;

    /**
     *  Does all the animation/drawing for a state
     *  -> houses the content! of the game loop.
     *  Not the loop itself
     */
    virtual void update()=0;

};
#endif //CP16_STATE_H
