//
// Created by Knork on 07.08.2016.
//

#ifndef CP16_HIGHSCORESTATE_H
#define CP16_HIGHSCORESTATE_H


#include "State.h"
#include "../menu/Panel.h"


class LevelCompleteState: public State {

protected:
    Panel* left,*right;
    Panel* next,*re;
    Panel *menu, *restart;
    std::vector<Panel*> allPanels;

    const GLuint reID=0, nextID=1;

    GLuint curActive=1;

    Shader *unlitShader;

    glm::mat4 proj,view;

    Window * const window;

    GLuint amount;
    GLfloat diff;
    std::string time;

    bool gameOver;


    void onGameOver();
    void onDefault();
public:



    LevelCompleteState(Window *win);
    virtual ~LevelCompleteState();

    const static GLuint LEFT =2;
    const static GLuint RIGHT =1;
    const static GLuint ENTER =0;

    virtual void setActive() override;

    virtual void setDeactive() override;

    virtual void update() override;

    void setGameOver(bool in);
    void handleInput(GLuint key);
};


#endif //CP16_HIGHSCORESTATE_H
