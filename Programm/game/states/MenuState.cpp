//
// Created by Knork on 07.08.2016.
//

#include "MenuState.h"
#include "MenuInputHandler.h"
#include "../../helper/GLOBAL_VARS.h"
#include "../StateManager.h"
#include <thread>
#include <glm/gtc/matrix_transform.hpp>

MenuState::MenuState(Window *window):window(window) {

    background = new Panel("model/textures/menu/menuBackground.jpg",glm::vec3(0.0f,1.0f,-3.0f),1.00f,(16.0f)/(9.0f),window);
    allPanels.push_back(background);

    play = new Panel("model/textures/menu/ButtonPlay.png",glm::vec3(0.68f,0.85f,00.0f),0.20f,(760.0f)/(443.0f),window);
    play->setActive(true);
    allPanels.push_back(play);

    exit = new Panel("model/textures/menu/ButtonExit.png",glm::vec3(0.68f,0.4f,00.0f),0.20f,(760.0f)/(443.0f),window);
    exit->setActive(false);
    allPanels.push_back(exit);

    unlitShader = new Shader("shader/vunlitTexture.glsl","shader/funlitTexture.glsl");

    proj = glm::ortho(0.0f,(float)(window->screenWidth),0.0f,(float)window->screenHeight,-1000.0f,1000.0f);
//    view = glm::lookAt(glm::vec3(0.0f,0.0f,-10.0f),glm::vec3(0.0f,0.0f,0.0f),glm::vec3(0.0f,1.0f,0.0f));

    MenuInput::MenuInput(window,this);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

MenuState::~MenuState() {
    delete unlitShader;
    delete background;
    delete play;
    delete exit;
}

void MenuState::setActive() {
    MenuInput::setActive();
}

void MenuState::setDeactive() {

}

void MenuState::update() {

    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    unlitShader->use();
   glUniformMatrix4fv(glGetUniformLocation(unlitShader->Program,"proj"),1,GL_FALSE,&proj[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(unlitShader->Program,"view"),1,GL_FALSE,&view[0][0]);

    for(auto & p:allPanels){
        p->draw(unlitShader);
    }


    std::this_thread::__sleep_for(std::chrono::seconds(0),std::chrono::nanoseconds(5000000));

    glfwPollEvents();
    glfwSwapBuffers(window->getWindow());

}

void MenuState::handleInput(GLuint key) {

    if(key == UP){

        if(curActive==playID){
            play->setActive(false);
            exit->setActive(true);
            curActive=exitID;
        }else{
            play->setActive(true);
            exit->setActive(false);
            curActive=playID;
        }

    }
    else if(key == DOWN){

        if(curActive==playID){
            play->setActive(false);
            exit->setActive(true);
            curActive=exitID;
        }else{
            play->setActive(true);
            exit->setActive(false);
            curActive=playID;
        }



    }
    else if(key == ENTER){
        ASSERT(game::stageManager !=NULL , " state Manager in global file is NULL");

        if(curActive == playID){
            game::stageManager->setNextState(StateManager::GAME);
        }
        else if(curActive == exitID){
            glfwSetWindowShouldClose(window->getWindow(), GL_TRUE);
        }
    }

}

