//
// Created by Knork on 07.08.2016.
//

#include "MenuInputHandler.h"
#include "MenuState.h"

Window *MenuInput::window;
MenuState *MenuInput::menuState;



void MenuInput::MenuInput(Window * win,MenuState *menuState){
    ASSERT(win!= NULL,"Window in HighScoreInput is null...");
    MenuInput::window=win;
    MenuInput::menuState=menuState;
}

void MenuInput::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode) {

    ASSERT(menuState!=NULL,"Forgot to call MenuInput::HighScoreInput(...) ");

    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(key == GLFW_KEY_UP && action == GLFW_PRESS){
        menuState->handleInput(MenuState::UP);
    }

    if(key == GLFW_KEY_DOWN && action == GLFW_PRESS){
        menuState->handleInput(MenuState::DOWN);
    }
    if(key == GLFW_KEY_ENTER && action == GLFW_PRESS){
        menuState->handleInput(MenuState::ENTER);
    }

}

void MenuInput::setActive() {
    ASSERT(MenuInput::window!= NULL,"Forgot to call MenuInput::HighScoreInput(...) ");
    glfwSetKeyCallback(MenuInput::window->getWindow(),MenuInput::keyCallback);
}
