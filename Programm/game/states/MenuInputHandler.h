//
// Created by Knork on 07.08.2016.
//

#ifndef CP16_MENUINPUTHANDLER_H
#define CP16_MENUINPUTHANDLER_H

#include "../../helper/Window.h"
#include "MenuState.h"

namespace MenuInput{

    extern Window *window;
    extern MenuState * menuState;



    void MenuInput(Window * win,MenuState * menu);
    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
    void setActive();

}

#endif //CP16_MENUINPUTHANDLER_H
