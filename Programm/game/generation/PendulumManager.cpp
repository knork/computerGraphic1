//
// Created by Knork on 27.07.2016.
//

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/string_cast.hpp>

#include "PendulumManager.h"
#include "../../helper/GLOBAL_VARS.h"


void PendulumManager::createInstance(glm::mat4 transform, GLfloat actionsEveryNSeconds, GLfloat forceMultiplier) {

//    this->actionsEveryNSeconds.push_back(actionsEveryNSeconds);
    this->forceMulti.push_back(forceMultiplier);
    this->worldMatrix.push_back(transform);
    this->cur.push_back(inverseStart * transform);
//    ASSERT(actionsEveryNSeconds>0.0f,"0 action does not make sense on a moving obstacle");
    rotPerSec.push_back(2*glm::pi<GLfloat>()/actionsEveryNSeconds);
    curAngle.push_back(0.0f);
    upwards.push_back(true);

    allPlanes.insert(allPlanes.end(),bluePrintPlanes.begin(),bluePrintPlanes.end());

    lightsID.push_back(std::vector<GLuint>());
    for(auto &m:lightMatrices) {
        glm::vec3 pos = glm::vec3(transform * m * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
        // type, ambient, pos, spec
        lightsID[lightsID.size()-1].push_back(lights->addLight(Lights::point, glm::vec3(0.05f, 0.05f, 0.05f), pos, 120));
    }


}

PendulumManager::PendulumManager(std::string path,Lights* lights):Obstacle(path,lights) {
    for(auto & m:worldMatrix){
        m = glm::translate(m,glm::vec3(0.0f,8.0f,0.0f));
    }
    lights->addMeshes(lightMesh);
}

PendulumManager::~PendulumManager() {

}

void PendulumManager::draw(Shader *shader) {
    for( auto &mesh:lightMesh){
        mesh->bufferData(cur);
    }
//    for(auto &m:lightMesh) {
//        glm::vec3 pos = glm::vec3(cur[i] * m->getTransform() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
//
//    }

    Obstacle::draw(shader);

}

void PendulumManager::animate() {

//    std::vector<glm::mat4> rotMatrix;
//    rotMatrix.reserve(cur.size());



    glm::mat4 tmp;

    std::vector<glm::vec3> dir;


    for(size_t i =0;i<cur.size();i++) {
        tmp = glm::mat4(1.0f);
        if(upwards[i])
            curAngle[i] += rotPerSec[i] * game::deltaTime;
        else
            curAngle[i] -= rotPerSec[i] * game::deltaTime;

        tmp = glm::rotate(tmp, curAngle[i], glm::vec3(0.0f, 0.0f, 1.0f));



        cur[i] = worldMatrix[i] *start * tmp * inverseStart;

        glm::vec3 before = (lights->getLight(lightsID[i][0]).pos);

        /// for every instance update all lights
        for(size_t k =0;k<lightMesh.size();k++){
            glm::vec3 pos = glm::vec3(cur[i] * lightMesh[k]->getTransform() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
            lights->getLight(lightsID[i][k]).pos = pos;
        }
        glm::vec3 after = (lights->getLight(lightsID[i][0]).pos);
        dir.push_back(glm::normalize(after-before));

        if (curAngle[i] > glm::half_pi<GLfloat>()) {
            upwards[i]=false;
        }
        else if(curAngle[i]<-glm::half_pi<GLfloat>()){
            upwards[i]=true;
        }

    }



    // update collision planes

    size_t bppSize = bluePrintPlanes.size();

    // for every instance
    for(size_t i =0;i<cur.size();i++){
        size_t offset = i * bppSize;


        glm::mat4 transform =   cur[i];

        glm::mat4 inverse = glm::inverse(transform);
        glm::mat4 inverseTranspose = glm::inverse(glm::transpose(transform));






        // correct all planes
        for(size_t k =0;k<bppSize;k++){

            glm::vec3 norm = glm::normalize(glm::vec3(inverseTranspose * glm::vec4(bluePrintPlanes[k].normal, 0.0f)));
            // create a new Collision Plane instance
            // important! (A * B )^-1 = B^-1 * A^-1 !



            glm::mat4 curInverse = bluePrintPlanes[k].inverseTransform * inverse;
//            if(first)
//                std::cout << "Pendulum " << glm::to_string(curInverse) << std::endl;

//            glm::mat4 inv = bluePrintPlanes[k].inverseTransform*instanceInverse;
//
//            glm::mat4 invTrans = glm::inverse(glm::transpose(glm::inverse(inv)));
//
//            glm::vec3 norm =  glm::normalize(glm::vec3(invTrans * glm::vec4(allPlanes[offset+k].normal,0.0f)));
//            glm::mat4 curInverse = bluePrintPlanes[k].inverseTransform * inverse;

//            std::cout << " All Planes beofre   " << glm::to_string(allPlanes[offset+k].inverseTransform) << std::endl;


            allPlanes[offset+k].inverseTransform= curInverse;
            allPlanes[offset+k].normal = norm;
            allPlanes[offset+k].force = forceMulti[i];
            GLfloat d = std::fmax(glm::dot(norm,dir[i]),0.0f);
            allPlanes[offset+k].direction =d*dir[i];




        }
    }

}

std::vector<DynCollisionPlane> * PendulumManager::getPlanesPointer() {
    return &allPlanes;
}

void PendulumManager::reset() {

    forceMulti.clear();
    worldMatrix.clear();
    cur.clear();
    rotPerSec.clear();
    curAngle.clear();
    upwards.clear();
    allPlanes.clear();
    lightsID.clear();
}
