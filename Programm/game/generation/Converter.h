// David Eberly, Geometric Tools, Redmond WA 98052
// Copyright (c) 1998-2016
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 3.0.0 (2016/06/19)
// Edited to work with glm -- Knork (2016/08/01)

#include <GL/glew.h>
#include <glm/glm.hpp>

namespace convert
{
    
    class ConvertCoordinates
    {
    public:
        // Construction of the change of basis matrix.  The implementation
        // supports both linear change of basis and affine change of basis.
        ConvertCoordinates();

        // Compute a change of basis between two coordinate systems.  The return
        // value is 'true' iff U and V are invertible.  The matrix-vector
        // multiplication conventions affect the conversion of matrix
        // transformations.  The Boolean inputs indicate how you want the matrices
        // to be interpreted when applied as transformations of a vector.
        bool operator()(
                glm::mat4 const& U, bool vectorOnRightU,
                glm::mat4 const& V, bool vectorOnRightV);

        // Member access.
        inline glm::mat4 const& GetC() const;
        inline glm::mat4 const& GetInverseC() const;
        inline bool IsVectorOnRightU() const;
        inline bool IsVectorOnRightV() const;
        inline bool IsRightHandedU() const;
        inline bool IsRightHandedV() const;

        // Convert points between coordinate systems.  The names of the systems
        // are U and V to make it clear which inputs of operator() they are
        // associated with.  The X vector stores coordinates for the U-system and
        // the Y vector stores coordinates for the V-system.

        // Y = C^{-1}*X
        inline glm::vec4 UToV(glm::vec4 const& X) const;

        // X = C*Y
        inline glm::vec4 VToU(glm::vec4 const& Y) const;

        // Convert transformations between coordinate systems.  The outputs are
        // computed according to the tables shown before the function
        // declarations. The superscript T denotes the transpose operator.
        // vectorOnRightU = true:  transformation is X' = A*X
        // vectorOnRightU = false: transformation is (X')^T = X^T*A
        // vectorOnRightV = true:  transformation is Y' = B*Y
        // vectorOnRightV = false: transformation is (Y')^T = Y^T*B

        // vectorOnRightU  | vectorOnRightV  | output
        // ----------------+-----------------+---------------------
        // true            | true            | C^{-1} * A * C
        // true            | false           | (C^{-1} * A * C)^T
        // false           | true            | C^{-1} * A^T * C
        // false           | false           | (C^{-1} * A^T * C)^T
        glm::mat4 UToV(glm::mat4 const& A) const;

        // vectorOnRightU  | vectorOnRightV  | output
        // ----------------+-----------------+---------------------
        // true            | true            | C * B * C^{-1}
        // true            | false           | C * B^T * C^{-1}
        // false           | true            | (C * B * C^{-1})^T
        // false           | false           | (C * B^T * C^{-1})^T
        glm::mat4 VToU(glm::mat4 const& B) const;

    private:
        // C = U^{-1}*V, C^{-1} = V^{-1}*U
        glm::mat4 mC, mInverseC;
        bool mIsVectorOnRightU, mIsVectorOnRightV;
        bool mIsRightHandedU, mIsRightHandedV;
    };



    ConvertCoordinates::ConvertCoordinates()
            :
            mIsVectorOnRightU(true),
            mIsVectorOnRightV(true),
            mIsRightHandedU(true),
            mIsRightHandedV(true)
    {
            mC = glm::mat4(1.0f);
            mInverseC= glm::mat4(1.0f);;
    }

    bool ConvertCoordinates::operator()(
            glm::mat4 const& U, bool vectorOnRightU,
            glm::mat4 const& V, bool vectorOnRightV)
    {
            // Initialize in case of early exit.
            mC= glm::mat4(1.0f);
            mInverseC= glm::mat4(1.0f);
            mIsVectorOnRightU = true;
            mIsVectorOnRightV = true;
            mIsRightHandedU = true;
            mIsRightHandedV = true;

            glm::mat4 inverseU= glm::inverse(U);
            GLfloat determinantU= glm::determinant(U);
            // arbitray epsilon
            bool invertibleU = abs(determinantU) > 0.0001f;
            if (!invertibleU)
            {
                    return false;
            }

            glm::mat4 inverseV= glm::inverse(V);
            GLfloat determinantV= glm::determinant(V);
            bool invertibleV = abs(determinantV) > 0.0001f;
            if (!invertibleV)
            {
                    return false;
            }

            mC = inverseU * V;
            mInverseC = inverseV * U;
            mIsVectorOnRightU = vectorOnRightU;
            mIsVectorOnRightV = vectorOnRightV;
            mIsRightHandedU = (determinantU > (GLfloat)0);
            mIsRightHandedV = (determinantV > (GLfloat)0);
            return true;
    }

    inline
    glm::mat4 const& ConvertCoordinates::GetC() const
    {
            return mC;
    }

    inline
    glm::mat4 const& ConvertCoordinates::GetInverseC() const
    {
            return mInverseC;
    }

    inline
    bool ConvertCoordinates::IsVectorOnRightU() const
    {
            return mIsVectorOnRightU;
    }

    inline
    bool ConvertCoordinates::IsVectorOnRightV() const
    {
            return mIsVectorOnRightV;
    }

    inline
    bool ConvertCoordinates::IsRightHandedU() const
    {
            return mIsRightHandedU;
    }

    inline
    bool ConvertCoordinates::IsRightHandedV() const
    {
            return mIsRightHandedV;
    }

    inline
    glm::vec4 ConvertCoordinates::UToV(glm::vec4 const& X)
    const
    {
            return mInverseC * X;
    }

    inline
    glm::vec4 ConvertCoordinates::VToU(glm::vec4 const& Y)
    const
    {
            return mC * Y;
    }

    
    glm::mat4 ConvertCoordinates::UToV(
            glm::mat4 const& A) const
    {
            glm::mat4 product;

            if (mIsVectorOnRightU)
            {
                    product = mInverseC * A * mC;
                    if (mIsVectorOnRightV)
                    {
                            return product;
                    }
                    else
                    {
                            return glm::transpose(product);
                    }
            }
            else
            {
//                    product = mInverseC * MultiplyATB(A, mC);
                    product = mInverseC * (glm::transpose(A) * mC);
                    if (mIsVectorOnRightV)
                    {
                            return product;
                    }
                    else
                    {
                            return glm::transpose(product);
                    }
            }
    }


    glm::mat4 ConvertCoordinates::VToU(
            glm::mat4 const& B) const
    {
            // vectorOnRightU  | vectorOnRightV  | output
            // ----------------+-----------------+---------------------
            // true            | true            | C * B * C^{-1}
            // true            | false           | C * B^T * C^{-1}
            // false           | true            | (C * B * C^{-1})^T
            // false           | false           | (C * B^T * C^{-1})^T
            glm::mat4 product;

            if (mIsVectorOnRightV)
            {
                    product = mC * B * mInverseC;
                    if (mIsVectorOnRightU)
                    {
                            return product;
                    }
                    else
                    {
                            return glm::transpose(product);
                    }
            }
            else
            {
//                    product = mC * MultiplyATB(B, mInverseC);
                    product = mC * (glm::transpose(B) * mInverseC);
                    if (mIsVectorOnRightU)
                    {
                            return product;
                    }
                    else
                    {
                            return glm::transpose(product);
                    }
            }
    }


}
