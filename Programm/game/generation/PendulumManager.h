//
// Created by Knork on 27.07.2016.
//

#ifndef CP16_PENDULUMMANAGER_H
#define CP16_PENDULUMMANAGER_H

#include <string>
#include <glm/glm.hpp>
#include <vector>
#include "../../helper/CollisionData.h"
#include "Obstacle.h"



class PendulumManager:public Obstacle{

protected:

    std::vector<std::vector<GLuint>> lightsID;

    std::vector<GLfloat> forceMulti,rotPerSec;
    std::vector<GLfloat> curAngle;
    std::vector<bool> upwards;

    std::vector<DynCollisionPlane> allPlanes;



public:
    virtual void animate() override;

    PendulumManager(std::string path,Lights* lights);
    virtual ~PendulumManager();
    /**
     * Later iterations may include parameters for force and swing-time
     */
    virtual void createInstance(glm::mat4 transform, GLfloat actionsEveryNSeconds, GLfloat forceMultiplier) override;

    std::vector<DynCollisionPlane> * getPlanesPointer();

    virtual void draw(Shader *shader) override;

    virtual void reset() override;
};


#endif //CP16_PENDULUMMANAGER_H
