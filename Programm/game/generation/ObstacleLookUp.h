//
// Created by Knork on 27.07.2016.
//

#ifndef CP16_OBSTACLELOOKUP_H
#define CP16_OBSTACLELOOKUP_H



#include <string>
#include "Obstacle.h"
#include "../../helper/Shader.h"
#include "../../helper/Lights.h"

/**
 *
 */
class ObstacleLookUp {

    Lights *lights;
    std::vector<std::string> names;
    std::vector<Obstacle*> allObstacleTypes;
    std::vector<std::vector<DynCollisionPlane>*> allPlanes;


public:

    ObstacleLookUp();
    ~ObstacleLookUp();

    void animateAll();

    void drawAll(Shader* shader);

    /**
     *
     * @param name - obstacle name (read in from XML)
     * @return obstacle pointer (used to create instances)
     */
    Obstacle* lookUp(std::string name);

    std::vector<std::vector<DynCollisionPlane>*>* getAllPlanes();

    /**
     *
     * @return vector of all availible Obstacles
     */
    std::vector<std::string>& getNames();

    Lights* getLights();


    void resetAll();
};


#endif //CP16_OBSTACLELOOKUP_H
