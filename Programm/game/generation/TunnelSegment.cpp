//
// Created by Knork on 09.07.2016.
// CopyRight Notice: Used rapid_xml docu to parse XML
// http://rapidxml.sourceforge.net/manual.html
//

#include <assimp/Importer.hpp>
//#include <iostream>
//
#include "TunnelSegment.h"
#include <rapidXML/rapidxml.hpp>
//
#include <rapidXML/rapidxml_print.hpp>
#include "TunnelSegmentParser.h"

TunnelSegment::TunnelSegment(std::string path, ObstacleLookUp *ol) {
    olu = ol;
    loadModel(path);
}

void TunnelSegment::loadModel(std::string path) {

    std::size_t found = path.find_last_of('/');
    this->directory = path.substr(0, found);
    using namespace rapidxml;
    xml_document<> doc;
    xml_node<> *root_node;
    // Read the xml file into a vector
    ifstream theFile(path.c_str());
    /// buffer needs to stay alive during the whole parsing process -> easierst to keep it "unextracted"
    /// not the nicest solution...
    vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
    buffer.push_back('\0');
    // Parse the buffer using the xml file parsing library into doc

    doc.parse<0>(&buffer[0]);

    // Find our root node
    root_node = doc.first_node("TunnelSegment");
    ASSERT(root_node, "XML file missing/wrong path" << path << ";");



    xml_node<> *cur;
    Lights* l = olu->getLights();
    for (xml_node<> *meshNode = root_node->first_node("mesh"); meshNode; meshNode = meshNode->next_sibling(
            "mesh")) {
        /// Dev note: parse Mesh create a instance and this object will be it's owner!
        /// This object is responsible to delete the mesh


        xml_attribute<>* attr = meshNode->first_attribute("name");
        if(attr && strstr(attr->value(),"light")){

            InstanceMesh *m  = TSParser::pareMesh(meshNode, directory);
            lightMatrices.push_back(m->getTransform());
            lightMesh.push_back(m);
        }else{
            InstanceMesh *m  = TSParser::parseMeshFourTextures(meshNode, directory);
            meshes.push_back(m);
        }

    }



    for (xml_node<> *planeNode = root_node->first_node("plane"); planeNode; planeNode = planeNode->next_sibling(
            "plane")) {
        collisionPlanes.push_back(TSParser::parsePlane(planeNode));
    }

    cur = root_node->first_node("meta");

    dif = std::atoi(cur->first_node("difficulty")->value());
    type = std::atoi(cur->first_node("type")->value());
    rot = std::atof(cur->first_node("rot")->value());
    cur = cur->first_node("mat4");
    ASSERT(cur, "stat node is missing");
    start = TSParser::parseMat4(cur);
    ASSERT(cur, "next node is missing");
    cur = cur->next_sibling("mat4");
    next = TSParser::parseMat4(cur);

    inverseStart = glm::inverse(start);
    correctedNext = inverseStart * next;

    std::vector<std::string>& names = olu->getNames();

    glm::mat4 transform;
    for (xml_node<> *node = root_node->first_node("mat4"); node; node = node->next_sibling("mat4")) {

        std::string type = (node->first_attribute("type"))->value();
        std::size_t found = type.find_last_of('.');
        if(found != std::string::npos)
            type = type.substr(0,found);
        // remove the _
        if(std::find(names.begin(),names.end(),type)!= names.end()){
            // type exists in our lookUp system
            transform = TSParser::parseMat4(node);
            obstacles.push_back({type,transform});
        }
    }



}




void TunnelSegment::createInstance(glm::mat4 transform, PlayerCollisionManager *pcm, GLfloat speed) {

    transform = inverseStart * transform;
    modelMatrices.push_back(transform);

    glm::mat4 inverse = glm::inverse(transform);
    glm::mat4 inverseTranspose = glm::inverse(glm::transpose(transform));



    for (size_t i = 0; i < collisionPlanes.size(); i++) {


        glm::vec3 norm = glm::normalize(glm::vec3(inverseTranspose * glm::vec4(collisionPlanes[i].normal, 0.0f)));
        // create a new Collision Plane instance
        // important! (A * B )^-1 = B^-1 * A^-1 !
        glm::mat4 curInverse = collisionPlanes[i].inverseTransform * inverse;


        pcm->add(CollisionPlane{norm, collisionPlanes[i].xmin, collisionPlanes[i].xmax,
                                collisionPlanes[i].zmin, collisionPlanes[i].zmax, curInverse});
    }

    // TODO dependend on difficulty cvhange parameters

//    std::cout << "tunnelSeg - create - obstacle size "<<obstacles.size()<< "  , " <<std::endl;
    for (auto &o:obstacles) {
        olu->lookUp(o.name)->createInstance(transform * o.transform, speed* (0.95 + 0.1*(11/(dif+1))), 2.0f);
    }



    for(auto &m:lightMesh) {
        glm::vec3 pos = glm::vec3(transform*m->getTransform()* glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
        // type, ambient, pos, spec
        olu->getLights()->addLight(Lights::point, glm::vec3(0.05f, 0.05f, 0.05f), pos, 120);
    }

}

#ifndef NDEBUG
bool isSetup = false;
#endif

void TunnelSegment::setup() {
#ifndef NDEBUG
    isSetup = true;
#endif

    for (std::size_t i = 0; i < meshes.size(); i++) {
        ASSERT(meshes[i], "PlayerMesh pointer has been deleted at some point meshnr: " << i);
        meshes[i]->setUpModelMatrices(modelMatrices, false);
    }
    for(auto&o:lightMesh){
        o->setUpModelMatrices(modelMatrices,false);
    }

    olu->getLights()->addMeshes(lightMesh);


}

void TunnelSegment::draw(Shader *shader) {
    ASSERT(shader != NULL, "Shader is NULL");
    ASSERT(isSetup,
           "Before draw, setup() needs to be called to signalize that you are done instanciating this Segment");
    for (std::size_t i = 0; i < meshes.size(); i++) {

        meshes[i]->draw(shader);
    }

}


/**
 *  to prevent delition of the meshes on move
 */

TunnelSegment::TunnelSegment(TunnelSegment &&move) {

    olu = move.olu;
    move.olu = NULL;

    collisionPlanes = std::move(move.collisionPlanes);
    meshes = std::move(move.meshes);
    modelMatrices = std::move(move.modelMatrices);
    obstacles = std::move(move.obstacles);
    textures_loaded = std::move(move.textures_loaded);
    lightMatrices = std::move(move.lightMatrices);
    lightMesh = std::move(move.lightMesh);

    directory = move.directory;
    dif = move.dif;
    type = move.type;
    next = move.next;
    start = move.start;
    inverseStart = move.inverseStart;
    correctedNext = move.correctedNext;
    rot = move.rot;


    for (size_t i = 0; i < move.meshes.size(); i++) {
        {
            move.meshes[i] = NULL;
        }

    }

}

TunnelSegment::~TunnelSegment() {

    for (InstanceMesh *m:meshes) {
        delete m;
    }

}


void TunnelSegment::resetInstances() {

    modelMatrices.clear();

    for(int i =0;i<meshes.size();i++){
            meshes[i]->reset();
    }

    for(int i =0;i<lightMesh.size();i++){
            lightMesh[i]->reset();
    }

}
