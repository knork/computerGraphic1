//
// Created by Knork on 26.08.2016.
//

#ifndef CP16_SCORE_H
#define CP16_SCORE_H


#include <GL/glew.h>
#include "../menu/StringRenderer.h"
#include <string>
#include <glm/vec4.hpp>
#include "../../helper/GLOBAL_VARS.h"

class Score {
    StringRenderer * stringRenderer;
    GLfloat score=0,cur=0,delta=0;

    GLuint highScore;

    void readHighScore();


public:
    Score();
    /**
     * Sets the score to desired level (used for reset)
     * @param in
     */
    void set(GLuint in);
    void add(GLuint in);
    void draw();
    void writeScore();
    GLuint getHighScore();
    GLuint getCount();

};


#endif //CP16_SCORE_H
