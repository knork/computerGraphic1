//
// Created by Knork on 27.07.2016.
//

#ifndef CP16_OBSTACLE_H
#define CP16_OBSTACLE_H

class Shader;

#include <glm/glm.hpp>
#include<vector>
#include <string>
#include <glm/detail/type_mat.hpp>
#include <GL/glew.h>
#include "../../helper/CollisionData.h"
#include "DynMesh.h"
#include "../../helper/Lights.h"


/**
 * Abstract class
 */
class Obstacle{

protected:

    Lights * const lights;

    /// transform that was loaded from the file
    glm::mat4 start, inverseStart;
    /// transform that places the obstacle in the world, set by create instance
    std::vector<glm::mat4> worldMatrix,cur;
    std::vector<DynCollisionPlane> bluePrintPlanes;
    std::vector<InstanceMesh*> meshes;
    std::vector<glm::mat4>lightMatrices;
    std::vector<InstanceMesh*> lightMesh;
    std::string directory;

    virtual void loadPath(std::string path);

public:
    Obstacle(std::string path,Lights *lights);
    virtual ~Obstacle(){for(auto * p:meshes) delete p; }
    virtual void animate()=0;
    virtual void createInstance(glm::mat4 transform,GLfloat actionsEveryNSeconds, GLfloat forceMultiplier)=0;
    virtual void draw(Shader *shader);
    virtual std::vector<DynCollisionPlane> * getPlanesPointer()=0;
    virtual void reset() =0;
};

#endif //CP16_OBSTACLE_H
