//
// Created by Knork on 24.07.2016.
//


#ifndef STBI_IMPORT
#define STBI_IMPORT
#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
/// important Node:
/// stb can only be loaded into a SINGLE file
/// -> all functions that use stb are to be written here
#include <stb/stb.h>
#include <stb/stb_image.h>
#endif


#include <fstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>

#include "TunnelSegmentParser.h"

#include "Converter.h"
#include "ObstacleLookUp.h"
#include "../../helper/GLOBAL_VARS.h"

/*
 * to ensure that there is only one copy of textures_loaded
 * it has to be placed in a separate from the header
 */

std::vector<Texture> TSParser::textures_loaded;


/**
    * Converts a vec3 from z-Up to y-Up Coords
    * Simply flips the y and z attribute
    */
glm::vec3 TSParser::toYup(glm::vec3 in) {
    in.x = -in.x;
    GLfloat tmp = in.y;
    in.y = in.z;
    in.z = tmp;
    return in;
}

/**
 * Converts a vec4 from z-Up to y-Up Coords
 * Simply flips the y and z attribute
 */
glm::vec4 TSParser::toYup(glm::vec4 in) {
    in.x = -in.x;
    GLfloat tmp = in.y;
    in.y = in.z;
    in.z = tmp;
    return in;
}

/**
 * Converts a mat4 from z-Up to y-Up Coords
 * Simply flips the y and z attribute
 */
glm::mat4 TSParser::toYup(glm::mat4 in) {
    // http://www.gamedev.net/topic/537664-handedness/#entry4480987


    convert::ConvertCoordinates converter;

    glm::mat4 glSystem;

    glSystem[0] = glm::vec4(1.0f, 0.0f, 0.0, 0.0f);
    glSystem[1] = glm::vec4(0.0f, 1.0f, 0.0, 0.0f);
    glSystem[2] = glm::vec4(0.0f, 0.0f, 1.0, 0.0f);
    glSystem[3] = glm::vec4(0.0f, 0.0f, 0.0, 1.0f);


    glm::mat4 input;

    input[0] = glm::vec4(-1.0f, 0.0f, 0.0, 0.0f);
    input[1] = glm::vec4(0.0f, 0.0f, 1.0, 0.0f);
    input[2] = glm::vec4(0.0f, 1.0f, 0.0, 0.0f);
    input[3] = in[3];

    converter(glSystem, true, input, false);
    glm::mat4 out = converter.VToU(in);
    return out;
}


glm::vec3 TSParser::parseVec3(xml_node<> *vec3Node) {
    ASSERT(vec3Node != NULL, "Vec3 node is null");

    std::string content(vec3Node->value());
    std::string delimiter = ",";
    size_t pos = 0;
    std::string token;

    glm::vec3 out;

    for (size_t i = 0; i < 3; i++) {
        pos = content.find(delimiter);
        token = content.substr(0, pos);

        out[i] = std::stof(token);
        content.erase(0, pos + delimiter.length());
    }
    return toYup(out);
}

glm::vec4 TSParser::parseVec4(xml_node<> *vec4Node) {

    ASSERT(vec4Node, "vec4 Node is null");

    std::string content(vec4Node->value());
    std::string delimiter = ",";
    size_t pos = 0;
    std::string token;

    glm::vec4 out;

    for (size_t i = 0; i < 4; i++) {
        pos = content.find(delimiter);
        token = content.substr(0, pos);
        out[i] = std::stof(token);
        content.erase(0, pos + delimiter.length());
    }
    return toYup(out);
}

glm::mat4 TSParser::parseMat4(xml_node<> *mat4Node) {

    ASSERT(mat4Node, "Mat4 Node is null");

    xml_node<> *vec = mat4Node->first_node("vec4");
    xml_node<> *entryNode;
    glm::mat4 out;

    /// GLM matrix is COL major!! remember remember ...

    for (size_t i = 0; i < 4; i++) {

        std::string content(vec->value());
        std::string delimiter = ",";
        size_t pos = 0;
        std::string token;

        glm::vec4 curVec;


        for (size_t i = 0; i < 4; i++) {
            pos = content.find(delimiter);
            token = content.substr(0, pos);
            curVec[i] = std::stof(token);
            content.erase(0, pos + delimiter.length());
        }

        // col major...
        out[0][i] = curVec[0];
        out[1][i] = curVec[1];
        out[2][i] = curVec[2];
        out[3][i] = curVec[3];
        vec = vec->next_sibling("vec4");
    }

    return toYup(out);

}


GLfloat min(GLfloat a, GLfloat b, GLfloat c, GLfloat d) {
    return std::fmin(std::fmin(c, d), std::fmin(a, b));
}

GLfloat max(GLfloat a, GLfloat b, GLfloat c, GLfloat d) {
    return std::fmax(std::fmax(c, d), std::fmax(a, b));
}

/**
 * Parses a Cplane given the correct Node
 */
CollisionPlane TSParser::parsePlane(xml_node<> *planeNode) {

    ASSERT(std::string(planeNode->name()).compare("plane") == 0, "passed node is not a plane!");

    glm::mat4 transform = parseMat4(planeNode->first_node("mat4"));
//    std::cout << std::endl <<  glm::to_string(transform) << std::endl;

    glm::vec3 a, b, c, d;

    xml_node<> *vec = planeNode->first_node("vec3");
    a = parseVec3(vec);

    vec = vec->next_sibling("vec3");
    b = parseVec3(vec);

    vec = vec->next_sibling("vec3");
    c = parseVec3(vec);

    vec = vec->next_sibling("vec3");
    d = parseVec3(vec);

    GLfloat xmin, xmax, zmin, zmax;

    xmin = min(a.x, b.x, c.x, d.x);
    xmax = max(a.x, b.x, c.x, d.x);

    zmin = min(a.z, b.z, c.z, d.z);
    zmax = max(a.z, b.z, c.z, d.z);




    // create normal

    glm::mat4 transposeInverse = glm::inverse(glm::transpose(transform));

    // calculate the normal in worldspace
    glm::vec3 normal = glm::normalize(glm::vec3(transposeInverse * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f)));


    glm::mat4 inverse = glm::inverse(transform);

    CollisionPlane cp{normal, xmin, xmax, zmin, zmax, inverse};


    return cp;
}

/**
 * This and the next method have been mostly copied from:
 * http://learnopengl.com/code_viewer.php?code=model&type=header
 */
GLint TSParser::loadTextureFromFile(const char *name, std::string directory) {
    //Generate texture ID and load texture data
    std::string filename = std::string(name);

    filename = directory + '/' + filename;

    const char * some = filename.c_str();

    GLuint textureID;
    glGenTextures(1, &textureID);
    int width, height;

    unsigned char *image = stbi_load(filename.c_str(), &width, &height, 0, STBI_rgb_alpha);

    ASSERT(image!=NULL,"image could not be loaded " << filename.c_str());
    // Assign texture to ID
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(image);
    return textureID;
}

Texture TSParser::createTexture(std::string filename, std::string dirPath, std::string type) {

    bool skip = false;
    Texture out;

    for (size_t i = 0; i < TSParser::textures_loaded.size(); i++) {
        // TODO compare absolute paths
        if (TSParser::textures_loaded[i].path == std::string(filename)) {
            // texture was loaded before!
            TSParser::textures_loaded.push_back(TSParser::textures_loaded[i]);
            out = TSParser::textures_loaded[i];
            skip = true;
            break;
        }
    }
    if (!skip) {
        Texture texture;
        texture.id = loadTextureFromFile(filename.c_str(), dirPath);
        texture.type = type;
        texture.path = filename;
        TSParser::textures_loaded.push_back(texture);
        out = texture;
    }

    return out;

}



std::vector<Texture> loadFourMaterialTextures(xml_node<> *meshNode, std::string dirPath) {

    std::vector<Texture> out;

    std::string path;

    // read texture path from File
    for (xml_node<> *diffuseNode = meshNode->first_node(
            "diffuse"); diffuseNode; diffuseNode = diffuseNode->next_sibling("diffuse")) {
        path = diffuseNode->value();
//        out.push_back(TSParser::createTexture(path, dirPath, "texture_diffuse"));

    }

    std::size_t found = path.find_last_of('/');
    if (found != std::string::npos) {
        std::string fileName = path.substr(found,path.size()-found-4);
        path = path.substr(0, found);

        dirPath+= path;
        /// We expect all textures to have this naming convetion and provide all of the below
        out.push_back(TSParser::createTexture(fileName+"_COLOR.png", dirPath, "texture_diffuse"));
        out.push_back(TSParser::createTexture(fileName+"_SPEC.png", dirPath, "texture_specular"));
        out.push_back(TSParser::createTexture(fileName+"_NRM.png", dirPath, "texture_normal"));
        out.push_back(TSParser::createTexture(fileName+"_DISP.png", dirPath, "texture_depth"));


    }
    return out;

}

std::vector<Texture> loadMaterialTextures(xml_node<> *meshNode, std::string dirPath) {

    std::vector<Texture> out;

    std::string path;

    // read texture path from File
    for (xml_node<> *diffuseNode = meshNode->first_node(
            "diffuse"); diffuseNode; diffuseNode = diffuseNode->next_sibling("diffuse")) {
        path = diffuseNode->value();



        std::size_t found = path.find_last_of('/');

        if (found != std::string::npos) {
            std::string fileName = path.substr(found,path.size()-found-4);
            path = path.substr(0, found);
            dirPath+=path;

            out.push_back(TSParser::createTexture(fileName+".png", dirPath, "texture_diffuse"));
        }


    }

//    std::size_t found = path.find_last_of('/');
//    if (found != std::string::npos) {
//        std::string fileName = path.substr(found,path.size()-found-4);
//        std::cout << "fileN " <<path <<"   "<< fileName << std::endl;
//        path = path.substr(0, found);
//
//        dirPath+= path;
//
//
//
//        out.push_back(TSParser::createTexture(fileName+"_COLOR.png", dirPath, "texture_diffuse"));
//        out.push_back(TSParser::createTexture(fileName+"_SPEC.png", dirPath, "texture_specular"));
//        out.push_back(TSParser::createTexture(fileName+"_NRM.png", dirPath, "texture_normal"));
//
//
//    }

    return out;
}

Vertex parseVertex(xml_node<> *vertexNode) {
    /// vertex structure v,v,v,n,n,n,uv,uv, t,t,t

    Vertex out;

    std::string content = vertexNode->value();
    std::string delimiter = ",";
    std::string token;
    size_t pos = 0;


    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.Position[0] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.Position[1] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.Position[2] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    out.Position = TSParser::toYup(out.Position);

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.Normal[0] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.Normal[1] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.Normal[2] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    out.Normal = TSParser::toYup(out.Normal);

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.TexCoords[0] = (std::stof(token));
    content.erase(0, pos + delimiter.length());

    pos = content.find(delimiter);
    token = content.substr(0, pos);
    out.TexCoords[1] = (std::stof(token));

    return out;
}

PlayerMesh *::TSParser::parsePlayerMesh(xml_node<> *meshNode, std::string dirPath) {


    xml_node<> *indicesNode = meshNode->first_node("indices");
    size_t amount = std::stoi(indicesNode->first_attribute("amount")->value());

    std::vector<GLuint> indices;

    std::string content = indicesNode->value();
    std::string delimiter = ",";
    std::string token;
    size_t pos = 0;
    for (size_t i = 0; i < amount * 3; i++) {
        pos = content.find(delimiter);
        token = content.substr(0, pos);

        indices.push_back(std::stoi(token));
        content.erase(0, pos + delimiter.length());
    }

    std::vector<Vertex> vertices;

    for (xml_node<> *vertex = meshNode->first_node("vertex"); vertex; vertex = vertex->next_sibling("vertex")) {
        vertices.push_back(parseVertex(vertex));
    }
    calculateTangents(&vertices,&indices);

    std::vector<Texture> textures = loadFourMaterialTextures(meshNode, dirPath);


    return new PlayerMesh(vertices, indices, textures);

}

InstanceMesh *TSParser::pareMesh(xml_node<> *meshNode, std::string dirPath) {

    xml_node<> *indicesNode = meshNode->first_node("indices");
    size_t amount = std::stoi(indicesNode->first_attribute("amount")->value());

    std::vector<GLuint> indices;

    std::string content = indicesNode->value();
    std::string delimiter = ",";
    std::string token;
    size_t pos = 0;
    for (size_t i = 0; i < amount * 3; i++) {
        pos = content.find(delimiter);
        token = content.substr(0, pos);

        indices.push_back(std::stoi(token));
        content.erase(0, pos + delimiter.length());
    }


    xml_node<> *transformNode = meshNode->first_node("mat4");
    glm::mat4 transform = parseMat4(transformNode);

    std::vector<Vertex> vertices;



    for (xml_node<> *vertex = meshNode->first_node("vertex"); vertex; vertex = vertex->next_sibling("vertex")) {
        vertices.push_back(parseVertex(vertex));
    }
    std::vector<Texture> textures = loadMaterialTextures(meshNode, dirPath);


    return new InstanceMesh(vertices, indices, textures, transform);
}

void TSParser::calculateTangents(std::vector<Vertex> *inOut, std::vector<GLuint> *indices) {

    // calculate tangents for normal mapping
    // idea for tangent calc from : https://www.youtube.com/watch?v=4FaWLgsctqY
    for(int i =0;i< indices->size();i+=3){

        // each triangle shares the same tangent
        GLuint i0 = indices->at(i);
        GLuint i1 = indices->at(i+1);
        GLuint i2 = indices->at(i+2);

        
        
        glm::vec3 edge0 = (*inOut).at(i1).Position - (*inOut)[i0].Position;
        glm::vec3 edge1 = (*inOut)[i2].Position - (*inOut)[i0].Position;

        GLfloat deltaU1 = (*inOut)[i1].TexCoords.x - (*inOut)[i0].TexCoords.x;
        GLfloat deltaV1 = (*inOut)[i1].TexCoords.y - (*inOut)[i0].TexCoords.y;
        GLfloat deltaU2 = (*inOut)[i2].TexCoords.x - (*inOut)[i0].TexCoords.x;
        GLfloat deltaV2 = (*inOut)[i2].TexCoords.y - (*inOut)[i0].TexCoords.y;

        GLfloat det = (deltaU1*deltaV2 - deltaU2*deltaV1);
        // we need 1/det so
        det = 1.0f/det;

        glm::vec3 tangent;
        tangent.x = det * (deltaV2*edge0.x - deltaV1 * edge1.x);
        tangent.y = det * (deltaV2*edge0.y - deltaV1 * edge1.y);
        tangent.z = det * (deltaV2*edge0.z - deltaV1 * edge1.z);

        (*inOut)[i0].Tangent+=tangent;
        (*inOut)[i1].Tangent+=tangent;
        (*inOut)[i2].Tangent+=tangent;


    }
    for(auto &vert:*inOut){
        vert.Tangent=glm::normalize(vert.Tangent);
    }
}

InstanceMesh *TSParser::parseMeshFourTextures(xml_node<> *meshNode, std::string dirPath) {

    xml_node<> *indicesNode = meshNode->first_node("indices");
    size_t amount = std::stoi(indicesNode->first_attribute("amount")->value());

    std::vector<GLuint> indices;

    std::string content = indicesNode->value();
    std::string delimiter = ",";
    std::string token;
    size_t pos = 0;
    for (size_t i = 0; i < amount * 3; i++) {
        pos = content.find(delimiter);
        token = content.substr(0, pos);

        indices.push_back(std::stoi(token));
        content.erase(0, pos + delimiter.length());
    }


    xml_node<> *transformNode = meshNode->first_node("mat4");
    glm::mat4 transform = parseMat4(transformNode);

    std::vector<Vertex> vertices;

    for (xml_node<> *vertex = meshNode->first_node("vertex"); vertex; vertex = vertex->next_sibling("vertex")) {
        vertices.push_back(parseVertex(vertex));
    }
    calculateTangents(&vertices,&indices);

    std::vector<Texture> textures = loadFourMaterialTextures(meshNode, dirPath);


    return new InstanceMesh(vertices, indices, textures, transform);
}
