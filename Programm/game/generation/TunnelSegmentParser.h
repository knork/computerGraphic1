//
// Created by Knork on 17.07.2016.
//

#ifndef CP16_TUNNELSEGMENTPARSER_H
#define CP16_TUNNELSEGMENTPARSER_H

#include <glm/detail/type_mat.hpp>
#include <rapidXML/rapidxml.hpp>
#include "../../helper/MeshStructs.h"
#include <vector>
#include <fstream>





#include "../../helper/ASSERT.h"
#include "../../helper/CollisionData.h"
#include "InstanceMesh.h"
#include "../../helper/PlayerMesh.h"

using namespace rapidxml;
namespace TSParser{

    struct ObstacleInstanceData{
        std::string name;
        glm::mat4 transform;
    };

    extern std::vector<Texture> textures_loaded;

    /**
     * Converts a vec3 from z-Up to y-Up Coords
     * Simply flips the y and z attribute
     */
    glm::vec3 toYup(glm::vec3 in);
    /**
     * Converts a vec4 from z-Up to y-Up Coords
     * Simply flips the y and z attribute
     */
     glm::vec4 toYup(glm::vec4 in);

    /**
     * Converts a mat4 from z-Up to y-Up Coords
     * Simply flips the y and z attribute
     */
    glm::mat4 toYup(glm::mat4 in);

    /**
     *
     * @param vec3Node from rapid xml
     * @return vec3 in y-up spacve
     */
     glm::vec3 parseVec3(xml_node<>* vec3Node);

    /**
     *
     * @param vec4Node from rapid xml
     * @return vec4 in y-up spacve
     */
    glm::vec4 parseVec4(xml_node<>* vec4Node);
    /**
     *
     * @param mat4Node - rapidxml node
     * @return matrix transformed to be in y-up space (right handed)
     */
    glm::mat4 parseMat4(xml_node<>* mat4Node);


    /**
     * Parses a Cplane given the correct Node
     */
    CollisionPlane parsePlane(xml_node<>* planeNode);

    /**
     * This and the next method have been mostly copied from:
     * http://learnopengl.com/code_viewer.php?code=model&type=header
     */

    /**
     * Parses a Mesh-Node where only a single texture is provided (currently used to
     * load light-models)
     *
     * @param meshNode
     * @param dirPath
     * @return caller is the owner! of the mesh
     */
    InstanceMesh* pareMesh(xml_node<> *meshNode, std::string dirPath);

    PlayerMesh* parsePlayerMesh(xml_node<> *meshNode, std::string dirPath);

    /** Parses a Mesh node that provides all texture Types (color,spec,norm,height)
     *
     * @param meshNode
     * @param dirPath - path to the file ... legacy should be removed
     * @return caller is the owner!
     */
    InstanceMesh* parseMeshFourTextures(xml_node<> *meshNode, std::string dirPath);

    /**
     *  All textures are identified by their name alone! not by their path!
     *  This is to reduce redundant loading of texture -> every textureNAME
     *  is loaded only once. ( Needs improvemnt that also considers the path.)
     *
     *
     * @param name -  name of the File e.g. 'pic.png'
     * @param directory - legacy fragment ... provide the directory e.g.  '../textures/'
     * @return Index to the texture
     */
    GLint loadTextureFromFile(const char *name, std::string directory);

    /**
     *  Should not be called individually. Is mainly used by the load TextureFromFile method
     *  Is only called if the requested Texture has nopt already been loaded into the GPU
     *
     * @param filename
     * @param dirPath
     * @param type
     * @return
     */
    Texture createTexture(std::string filename, std::string dirPath, std::string type);

    void calculateTangents(std::vector<Vertex> *inOut, std::vector<GLuint> *indices);

}

#endif //CP16_TUNNELSEGMENTPARSER_H
