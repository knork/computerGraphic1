//
// Created by Knork on 09.07.2016.
//

#ifndef CP16_DYNMESH_H
#define CP16_DYNMESH_H


#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

#include "../../helper/ASSERT.h"
#include "../../helper/MeshStructs.h"
#include "../../helper/Shader.h"
#include "InstanceMesh.h"
/**
 * Currently NOT in use!
 * May later be added to increase dynamic performance
 *
 */

class DynMesh : public InstanceMesh {

    std::vector<Vertex> vertices;


    std::vector<GLuint> indices;
    std::vector<Texture> textures;
    glm::mat4 transform;
    GLuint instanceCount = 0;
    GLuint buffer;
    /*  Render data  */
    GLuint VAO, VBO, EBO;


    void setupMesh();

public:
    /**
     * Create a blueprint for a mesh that, just like TunnelSegment, needs to be instanciated.
     * which is done by the TunnelSegment each time it creates a instance
     *
     * @param vertices - vertices(pos,norm,tex) of the mesh (in obj-coords)
     * @param indices - indices for drawing
     * @param textures - struct that containes the id of the (already loaded) texture
     * @param transform - placement relative to the Segment
     * @return Object that can be used to instanciate Meshes
     */
    DynMesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures,
               glm::mat4 transform);
    /// helper var to ensure that Buffers do not get deleted on move
    GLuint bufferAmount = 1;

    DynMesh(DynMesh &&move);

    ~DynMesh(){}





};

#endif //CP16_DYNMESH_H
