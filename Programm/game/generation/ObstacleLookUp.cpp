//
// Created by Knork on 27.07.2016.
//

#include "ObstacleLookUp.h"
#include "PendulumManager.h"
#include "../../helper/GLOBAL_VARS.h"


ObstacleLookUp::ObstacleLookUp() {

    lights = new Lights();
    game::obstacleLookup= this;
    allObstacleTypes.push_back(new PendulumManager("model/obstacles/pendulum/pendulum.xml",lights));
    names.push_back("Pendulum");
    allPlanes.push_back(allObstacleTypes[0]->getPlanesPointer());


}

void ObstacleLookUp::animateAll() {


    for(auto& obstManager:allObstacleTypes){
        /// updates light positions also
        obstManager->animate();
    }
    lights->update();
}

bool icompare_pred(unsigned char a, unsigned char b)
{
    return std::tolower(a) == std::tolower(b);
}
bool icompare(std::string const& a, std::string const& b)
{
    if (a.length()==b.length()) {
        return std::equal(b.begin(), b.end(),
                          a.begin(), icompare_pred);
    }
    else {
        return false;
    }
}

Obstacle* ObstacleLookUp::lookUp(std::string name) {
    for(size_t i =0;i< names.size();i++){
        if(icompare(name,names[i])){
            return allObstacleTypes[i];
        }
    }
    return nullptr;
}

std::vector<std::vector<DynCollisionPlane> *>* ObstacleLookUp::getAllPlanes() {
    return &allPlanes;
}

ObstacleLookUp::~ObstacleLookUp() {
    for(auto * d:allObstacleTypes){
        delete d;
        d= nullptr;
    }
    delete lights;
}

void ObstacleLookUp::drawAll(Shader *shader) {

    for(auto &o:allObstacleTypes)
        o->draw(shader);

    lights->draw();

}

std::vector<std::string>& ObstacleLookUp::getNames() {
    return names;
}

Lights *ObstacleLookUp::getLights() {
    return lights;
}

void ObstacleLookUp::resetAll() {
    for(auto &o:allObstacleTypes)
        o->reset();
}
