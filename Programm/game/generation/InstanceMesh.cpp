//
// Created by Knork on 25.07.2016.
//
// Copyright Notice ~60%: http://learnopengl.com/code_viewer.php?code=mesh&type=header
// Used for orientation, most of the File is however standart openGL code
// Also incoparation of instanced drawing: http://learnopengl.com/code_viewer.php?code=advanced/instancing_asteroids_instanced
// Filke: PlayerMesh.h, InstancedDrawingDemo.cpp
//

#include "InstanceMesh.h"

InstanceMesh::InstanceMesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures,
                           glm::mat4 transform) {
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;
    this->transform = transform;



// Now that we have all the required data, set the vertex buffers and its attribute pointers.
    setupMesh();
}


InstanceMesh::InstanceMesh(InstanceMesh &&move) {
    this->vertices = std::move(move.vertices);
    this->indices = std::move(move.indices);
    this->textures = std::move(move.textures);
    this->instanceCount = move.instanceCount;

    VAO = move.VAO;
    VBO = move.VBO;
    EBO = move.EBO;
    buffer = move.buffer;



// Make sure our buffer obj. stay valid!
    move.bufferAmount = 0;
}


InstanceMesh::~InstanceMesh() {
    glDeleteBuffers(bufferAmount, &buffer);
    glDeleteBuffers(bufferAmount, &VBO);
    glDeleteBuffers(bufferAmount, &EBO);
}


// Render all instances of this mesh
void InstanceMesh::draw(Shader *shader) {
    // Bind appropriate textures
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    for (GLuint i = 0; i < this->textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
        // Retrieve texture number (the N in diffuse_textureN)
        std::stringstream ss;
        std::string number;
        std::string name = this->textures[i].type;
        if (name == "texture_diffuse")
            ss << diffuseNr++; // Transfer GLuint to stream
        else if (name == "texture_specular")
            ss << specularNr++; // Transfer GLuint to stream

        number = ss.str();

//        std::cout << name+number << std::endl;

        // Now set the sampler to the correct texture unit
        glUniform1i(glGetUniformLocation(shader->Program, (name + number).c_str()), i);
        // And finally bind the texture
        glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
    }

    // Also set each mesh's shininess property to a default value (if you want you could extend this to another mesh property and possibly change this value)
    glUniform1f(glGetUniformLocation(shader->Program, "material.shininess"), 16.0f);

    // draw mesh
    glBindVertexArray(VAO);
    glDrawElementsInstanced(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0, instanceCount);

    glBindVertexArray(0);

    // Always good practice to set everything back to defaults once configured.
    for (GLuint i = 0; i < this->textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}


/**
 * Once we know how many instances we need
 * and where they are we can init the attribute Buffer for our
 * instance drawing
 *
 * Should not be called multiple times!... but i do because... well havn't figured out
 * how to do that probably...
 *
 */

void InstanceMesh::setUpModelMatrices(std::vector<glm::mat4> matrices, bool dynamic) {


    instanceCount = matrices.size();
    for (auto &m:matrices) {
        m = m * transform;
    }

    // Vertex Buffer Object
//    GLuint buffer;
    glBindVertexArray(VAO);
    glDeleteBuffers(1, &buffer);
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    if (dynamic)
        glBufferData(GL_ARRAY_BUFFER, matrices.size() * sizeof(glm::mat4), glm::value_ptr(matrices[0]),
                     GL_DYNAMIC_DRAW);
    else
        glBufferData(GL_ARRAY_BUFFER, matrices.size() * sizeof(glm::mat4), glm::value_ptr(matrices[0]), GL_STATIC_DRAW);
    // Vertex Attributes
    std::size_t vec4Size = sizeof(glm::vec4);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) 0);
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (vec4Size));
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (2 * vec4Size));
    glEnableVertexAttribArray(6);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (3 * vec4Size));

    glVertexAttribDivisor(3, 1);
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);

    glBindVertexArray(0);
}


// Initializes all the buffer objects/arrays
void InstanceMesh::setupMesh() {


    // Create buffers/arrays
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
    glGenBuffers(1, &this->EBO);

    glBindVertexArray(this->VAO);
    // Load data into vertex buffers
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
    // A great thing about structs is that their memory layout is sequential for all its items.
    // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
    // again translates to 3/2 floats which translates to a byte array.
    glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

    // Set the vertex attribute pointers
    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) 0);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, Normal));
    // Vertex Texture Coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, TexCoords));
    // 3,4,5,6 is used by the instance matrix
    glEnableVertexAttribArray(7);
    glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, Tangent));

    glBindVertexArray(0);

}

void InstanceMesh::bufferData(std::vector<glm::mat4> matrices) {

    /// painfully ineffient...
    setUpModelMatrices(matrices, true);

    // future only load buffer data.. for now stupid way


//    if(matrices.size()!=instanceCount){
//        setUpModelMatrices(matrices,true);
//    }else {
//
////    ASSERT(matrices.size()==instanceCount," You may not change the amount of instances without calling setUp() method");
//
//        glBindVertexArray(this->VAO);
//        glBindBuffer(GL_ARRAY_BUFFER, buffer);
//
//        glBufferData(GL_ARRAY_BUFFER, matrices.size() * sizeof(glm::mat4), glm::value_ptr(matrices[0]),
//                     GL_DYNAMIC_DRAW);
//
//
//
//
//
//        glBindVertexArray(0);
//
//    }
}

glm::mat4 InstanceMesh::getTransform() const {
    return transform;
}


void InstanceMesh::reset() {
    instanceCount=0;

}
