//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_TUNNELGENERATION_H
#define CP16_TUNNELGENERATION_H

class PlayerCollisionManager;

#include<vector>
#include<string>
#include<glm/glm.hpp>



#include "TunnelSegment.h"
#include "ObstacleLookUp.h"
#include "../menu/StringRenderer.h"
#include "../gameLoop/Timer.h"
#include "Score.h"

class TunnelGeneration {
    /**
     * Struct to track where points should be added to the score
     * This also assumes that the track goes along the z-axis (which it does currentlich)
     * but needs to be fixed if a more advanced track is generated
     */
    struct Points{
        GLfloat zVal;
        GLuint points;
    };

    /**
     * Strukt to make a compare function (easier than those weird c++11 features)
     */
    struct less_than_key
    {
        /// sorts points structs dependen on the distance to the start
        inline bool operator() (const Points& struct1, const Points& struct2)
        {
            return struct1.zVal < struct1.zVal;
        }
    };


    GLfloat difficulty; /// resets every on restart
    GLuint roundCounter=0; /// resets on restart

    StringRenderer *stringRenderer; /// not owned by this class
    Timer timer;
    GLfloat segmentCompletionTimeStamp;


    std::vector<std::string> segmentPaths;
    TunnelSegment *goal; /// special segment for start/finish

    /// stores all segments
    std::vector<TunnelSegment> segments;
    /// list of point marker
    std::vector<Points> pointMarks;
    GLuint pointIndex=0;

    /// just keeping some pointer to commonly used targets
    PlayerCollisionManager *pc;
    ObstacleLookUp *olu;


    std::default_random_engine generator;
    const static GLuint maxLives=3;
    GLuint lives=maxLives;/// resets on restart


    Shader *instanceShader;
    Score * score;

    /**
     * Simply keeps a sorted list of all availible segments
     * => if a segment is added programm needs to be recompiled
     * in future this list (among other data) should be read in
     *
     * Also they are sorted by difficulty (currently manually)
     */
    void fillsegmentPaths(){
        segmentPaths.push_back("model/segments/upDown/upDown.xml");
        segmentPaths.push_back("model/segments/curve/curve.xml");
        segmentPaths.push_back("model/segments/leftRight/leftRight.xml");
        segmentPaths.push_back("model/segments/upDown/upDownPend.xml");
        segmentPaths.push_back("model/segments/walkway/walkway.xml");
        segmentPaths.push_back("model/segments/jump/jump.xml");
        segmentPaths.push_back("model/segments/split/split.xml");
        segmentPaths.push_back("model/segments/split/split_hard.xml");

    }

    std::normal_distribution<float> normalDistr; /// segment index choosing
    std::uniform_real_distribution<float> speedDistr;

    GLuint updateDistribution(){
        /// max difficulty for segement is 8 at the moment, however obstacle parameters can still increase

        float mean = (difficulty/10) * segments.size();
        mean = fmin(mean, 11.0f);

        normalDistr = std::normal_distribution<float>(mean,segments.size()/4.0f);

        speedDistr = std::uniform_real_distribution<float>(0.9,1.2);

    }

    /**
     *
     * @return amount of segments for this stage at least 4
     */
    size_t getAmount(){
        int out;
        out = (int) (1.2f*difficulty - std::fabs(sin(difficulty*3.14f)*3.0f));
        out = max(out,4);
        return out;


    }

    /**
     *
     * @return speed at which obstacle should swing
     */
    GLfloat getSpeed(){
        return (7.0 + 0.8*difficulty)/(difficulty+1.0) * speedDistr(generator);
    }


    /**
     *
     * @return a index for the next Segment
     */
    int getSegmentIndex(){

        int out = (int) normalDistr(generator);
        out = std::max(out,0);
        out = std::min(out,(int)segments.size()-1);
        return out;

    }


public:

    GLfloat getDifficulty() const{
        return difficulty;
    }
    /**
     *
     * @param difficulty determains how hard the stage is going to be
     *  effects obstacle speed, and also what types of segments are going to be choosen
     */
    void makeTunnel(GLfloat difficulty){
        pointMarks.clear();
        roundCounter++;
        timer.reset();
        this->difficulty = difficulty;
        /// update segmentDifficultyDistribution for the new level!
        updateDistribution();

        /// every level has a start segment
        glm::mat4 curPlacement;
        curPlacement = glm::translate(curPlacement,glm::vec3(0.0f,0.0f,-6.0f));
        goal->createInstance(curPlacement, pc, 0);

        /// then the actual segments are being placed
        curPlacement = curPlacement * goal->getNext();
        for(std::size_t i=0;i<getAmount();i++){
//            for(std::size_t i=0;i<20;i++){
            unsigned int choosen =getSegmentIndex();
//                unsigned int choosen =2;
            segments[choosen].createInstance(curPlacement, pc, getSpeed());
            curPlacement = curPlacement * (segments[choosen].getNext());
            GLfloat z = (curPlacement * glm::vec4(0.0f,0.0f,0.0f,1.0f)).z;
            pointMarks.push_back({z,(segments[choosen].getDif()+1)*1000});
        }

        std::sort(pointMarks.begin(),pointMarks.end(),less_than_key());
        pointIndex=0;
        /// also every stage has a ending
        goal->createInstance(curPlacement, pc, 0);

        /// Creating the winning collision plane (maby extract to somehwere?)
            curPlacement = goal->getInverseStart() * curPlacement;
            glm::mat4 inverse = glm::inverse(curPlacement);
            glm::mat4 inverseTranspose = glm::inverse(glm::transpose(curPlacement));

            std::vector<CollisionPlane> collisionPlanes = goal->getCollisionPlanes();
            for (size_t i = 0; i < collisionPlanes.size(); i++) {
                glm::vec3 norm = glm::normalize(glm::vec3(inverseTranspose * glm::vec4(collisionPlanes[i].normal, 0.0f)));
                // create a new Collision Plane instance
                // important! (A * B )^-1 = B^-1 * A^-1 !
                glm::mat4 curInverse = collisionPlanes[i].inverseTransform * inverse;
                collisionPlanes[i]=CollisionPlane{norm, collisionPlanes[i].xmin, collisionPlanes[i].xmax,
                                        collisionPlanes[i].zmin, collisionPlanes[i].zmax, curInverse};
            }
            pc->setWinCollision(collisionPlanes);
            goal->setup();


        ///Set up all instances before starting to draw
        /// this updates the uniform buffer object
        for(auto &seg:segments){
            seg.setup();
        }
        /// this is used to include time into the score
        segmentCompletionTimeStamp = glfwGetTime();
    }

    /**
     * resets values that need to be reset at a restart
     */
    void restart(){
        score->set(0);
        lives = maxLives;
        difficulty=0;
        roundCounter=0;
    }

    /**
     * Does all the set up that should only be done once
     * e.g. load segments (from disk)
     *
     * @param pc
     * @param olu
     * @return
     */
    TunnelGeneration(PlayerCollisionManager *pc,ObstacleLookUp *olu):pc(pc),olu(olu){
        game::tunnelGeneration = this;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        generator = std::default_random_engine(seed);
        score = new Score();
        stringRenderer = game::stringRenderer;
        ASSERT(stringRenderer!=NULL,"stringRenderer is not initialized yet");

        fillsegmentPaths();
        for(auto &s:segmentPaths){
            // later diffrent container for diffrent difficulties
            segments.push_back(TunnelSegment(s, olu));
        }

        goal = new TunnelSegment("model/segments/Goal/goal.xml",olu);



        instanceShader = new Shader("shader/vInstanceShader.glsl","shader/fInstanceShader.glsl");

    }

    ~TunnelGeneration(){
        delete goal;
        delete instanceShader;
        delete score;
    }

    void updateScore(){
        if(pointIndex<pointMarks.size() && game::camera->Position.z - game::camera->offset.z> pointMarks[pointIndex].zVal){

            GLfloat duration = glfwGetTime() -  segmentCompletionTimeStamp;

            GLfloat timeBonus= std::fmin(1.0f,2.0f/(duration));

            score->add(pointMarks[pointIndex].points*timeBonus*roundCounter);
            pointIndex++;
            segmentCompletionTimeStamp = glfwGetTime();
        }
    }

    void draw(){

        updateScore(); /// doesn't really belong into a draw method....

        instanceShader->use();
        for(auto &seg:segments){
            seg.draw(instanceShader);
        }
        goal->draw(instanceShader);
        olu->drawAll(instanceShader);
        score->draw();
        timer.draw();
        stringRenderer->RenderText(std::to_string(lives),0.95,0.04,1.6,glm::vec4(0.43,0.0,0.0,0.9));
    }


    void retry(){
        timer.reset();
    }

    /**
     * Reset for a new stage but NOT a full game restart
     */
    void reset(){

        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        generator = std::default_random_engine(seed);

        pc->reset();
        olu->resetAll();
        for(auto &s:segments){
            s.resetInstances();
        }
        olu->getLights()->reset();
        goal->resetInstances();
    }

    GLuint getCurLevel(){
        return roundCounter;
    }


    Timer* getTimer(){
        return &timer;
    }

    GLuint& getLives(){
        return lives;
    }

    Score* getScore(){
        return score;
    }


};



#endif //CP16_TUNNELGENERATION_H
