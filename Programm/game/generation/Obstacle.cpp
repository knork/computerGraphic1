//
// Created by Knork on 27.07.2016.
//

#include "Obstacle.h"
#include "../../helper/ASSERT.h"
#include "TunnelSegmentParser.h"
#include <iostream>
#include <fstream>
#include <streambuf>

#include <rapidXML/rapidxml.hpp>

void Obstacle::loadPath(std::string path) {
    std::size_t found = path.find_last_of('/');
    std::string dir = path.substr(0,found);

    using namespace rapidxml;
    xml_document<> doc;
    xml_node<> *root_node;
    // Read the xml file into a vector
    std::ifstream theFile(path.c_str());
    std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
    buffer.push_back('\0');
    // Parse the buffer using the xml file parsing library into doc
    doc.parse<0>(&buffer[0]);
    // Find our root node
    root_node = doc.first_node("TunnelSegment");
    ASSERT(root_node, "XML file missing/wrong path,");

    xml_node<> *cur;

    for (xml_node<> *meshNode = root_node->first_node("mesh"); meshNode; meshNode = meshNode->next_sibling(
            "mesh")) {
        /// Dev note: parse Mesh create a instance and this object will be it's owner!
        /// This object is responsible to delete the mesh
        xml_attribute<>* attr = meshNode->first_attribute("name");
        if(attr && strstr(attr->value(),"light")){
            InstanceMesh *m  = TSParser::pareMesh(meshNode, dir);
            lightMatrices.push_back(m->getTransform());
            lightMesh.push_back(m);
        }else{
            InstanceMesh *m  = TSParser::parseMeshFourTextures(meshNode, dir);
            meshes.push_back(m);
        }

//        meshes.push_back(TSParser::pareMesh(meshNode, dir));
    }

    for (xml_node<> *planeNode = root_node->first_node("plane"); planeNode; planeNode = planeNode->next_sibling(
            "plane")) {
        bluePrintPlanes.push_back((DynCollisionPlane)TSParser::parsePlane(planeNode));
    }



    cur = root_node->first_node("meta");
    ASSERT(cur,"meta node is missing");

    cur = cur->first_node("mat4");
    ASSERT(cur,"stat node is missing");
    start = TSParser::parseMat4( cur );

    inverseStart = glm::inverse(start);


}

void Obstacle::draw(Shader *shader) {

    /// meshes change everyframe -> new setup on each frame!

    for( auto &mesh:meshes){
        mesh->bufferData(cur);
    }

    for( auto &mesh:meshes){
        mesh->draw(shader);
    }
}

Obstacle::Obstacle(std::string path,Lights *l):lights(l) {
    loadPath(path);

}


