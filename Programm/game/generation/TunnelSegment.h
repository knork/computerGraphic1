//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_TUNNELSEGMENT_H
#define CP16_TUNNELSEGMENT_H



#include "InstanceMesh.h"
#include "../../helper/Shader.h"
#include "../../helper/PlayerMesh.h"
#include "../../helper/CollisionData.h"
#include "../gameLoop/PlayerCollisionManager.h"
#include "../../helper/Lights.h"
#include "TunnelSegmentParser.h"


#include <GL/glew.h>

#include <vector>
#include <string>
#include <assimp/scene.h>
/**
 * This Object loads a TunnelSegment and all the nessesary data from an xml file.
 * The Models can be created in blender and exported with the provided script.
 *
 * The blender file can contain special objects that will be marked with a '_' at the start of there name
 *
 * _start : where does this segment start?
 *      empty object : positive y-direction defines the forward direction of the segment
 *      only transform matrix will be exported
 *
 *  _next : where does the next segment anker? empty object
 *      only transform matrix will be exported
 *
 * _cPlane* : defines a collision plane. Needs to be a plane (e.g. 4 vertices in a 2D space)
 *
 * * : everything that is not prenoted by '_' will be considered a mesh
 *
 * TODO test weather loading of specular/bump/parralex maps works
 *
 */



class TunnelSegment{
    // All data is stored in Object-space

    ObstacleLookUp *olu;


    glm::mat4 start,next, inverseStart,correctedNext;

    GLuint dif;
    GLuint type;
    GLfloat rot;

    std::vector<glm::mat4>lightMatrices;
    std::vector<glm::mat4>modelMatrices;

    std::vector<TSParser::ObstacleInstanceData> obstacles;
    std::vector<InstanceMesh*> meshes;
    std::vector<InstanceMesh*> lightMesh;
    std::string directory;
    std::vector<Texture> textures_loaded;
    std::vector<CollisionPlane> collisionPlanes;


    GLfloat getSpeed();


public:
    /**
     *
     * @param path - path from the exe to the segment xml file, that was exported from
     *          Blender with the export script.
     * @return Object that can be used to then create instances of this Segment.
     */
    TunnelSegment(std::string path, ObstacleLookUp *ol);

    /// It does not make sense to copy this obj (because your supposed to create instance with it)
    /// -> no copyConstructor

    TunnelSegment(TunnelSegment&& move);

    ~TunnelSegment();

    /**
     * Creates a instance of the loaded Segment at the specified transforms.
     *
     * @param transform defines the placement of the instance
     * @param pc defines where the instance will register it's collision data
     */
    void createInstance(glm::mat4 transform, PlayerCollisionManager *pc, GLfloat speed);

    /**
     *  HAS to be called after all wanted instances have been registered (via the createInstance method)
     *
     *  It should be possible to create further instances however they will only be displayed after another
     *  call to this function
     */
    void setup();

    /**
     * Draws all instances of this Segment
     *
     * @param shader specifies the Shader that is used for drawing
     *
     */
    void draw(Shader* shader);



    /**
     * removes all isntances
     */
    void resetInstances();

private:
    /// loads and parses file from path
    void loadModel(std::string path);


public:

    /**
     *
     * @return placment to reach the start of the current segment
     */
    const glm::mat4 &getStart() const {
        return start;
    }
    /**
     *
     * @return placment for the next segment relative to the current one
     */
    const glm::mat4 &getNext() const {
        return correctedNext;
    }

    const glm::mat4 &getInverseStart() const {
        return inverseStart;
    }

    GLuint getDif() const {
        return dif;
    }

    GLuint getType() const {
        return type;
    }

    GLfloat getRot() const {
        return rot;
    }
    std::vector<CollisionPlane> getCollisionPlanes(){
            return collisionPlanes;
    }




};




#endif //CP16_TUNNELSEGMENT_H
