//
// Created by Knork on 26.08.2016.
//
#include "Score.h"




Score::Score() {
    stringRenderer = game::stringRenderer;
    readHighScore();
}

void Score::set(GLuint in) {
    score=in;
    cur=in;
    delta=0;
}

void Score::add(GLuint in) {



    score+=in;
    if(cur<score)
        delta= (score-cur)/2.0f;
    else
        delta=0;
}

void Score::readHighScore() {


    std::ifstream scoreFile ("score.txt");
    std::string line;
    if (scoreFile.is_open()) {

        getline(scoreFile,line);
        GLuint highScore = std::stoi(line);
        this->highScore=highScore;
    }else{
        highScore=0;
    }




}

GLuint Score::getCount() {
    return score;
}

void Score::draw() {

    if(cur<score){
        cur+=delta*game::deltaTime;
    }
    stringRenderer->RenderText(std::to_string((int)cur),0.45,0.04,1.6,glm::vec4(0.43,0.0,0.0,0.9));
}

void Score::writeScore() {

    if((unsigned int)score > highScore){
        std::ofstream scoreFile("score.txt");
        scoreFile << (unsigned int)score;
    }

}

GLuint Score::getHighScore() {
    return highScore;
}
