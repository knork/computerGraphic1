//
// Created by Knork on 07.08.2016.
//

#include "Panel.h"
#include "../generation/TunnelSegmentParser.h"
#include "../../helper/Window.h"


void Panel::fillVertices() {
    // Anchor should be top-left

    vertices = {
            {glm::vec3(0.0f,0.0f,0.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.0,1.0)}, // lower-left
            {glm::vec3(0.0f,1.0f,0.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.0,0.0)}, // upper-left
            {glm::vec3(1.0f,0.0f,0.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(1.0,1.0)}, // lower-right
            {glm::vec3(1.0f,1.0f,0.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(1.0,0.0)}}; // upper-right

    indices ={2,1,3,2,0,1};


}

Panel::Panel(std::string path, glm::vec3 pos, GLfloat size, GLfloat ratio,Window* win):window(win) {

    fillVertices();

    GLfloat x = window->screenWidth * pos.x;
    GLfloat y =   window->screenHeight * pos.y;

    glm::vec3 position(x,y,pos.z);

    // scaling
    x = win->screenWidth * size;
    y = x * (1/ratio);

    // apply scaling and translation
    for(auto & v:vertices){
        v.Position.x*= x;
        v.Position.y*=y;
        // and then move
        v.Position.x+= position.x;
        v.Position.y+= position.y -y;
        v.Position.z = pos.z;
    }



    // loading the Texture

    std::size_t found = path.find_last_of('/');
    std::string dir = path.substr(0, found);
    std::string file = path.substr(found);

    texture = TSParser::createTexture(file,dir,TEXTURE_DIFFUSE);

    // Loading stuff onto the GPU


    // Create buffers/arrays
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
    glGenBuffers(1, &this->EBO);

    glBindVertexArray(this->VAO);
    // Load data into vertex buffers
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

    // We know that we will always use 4 vertices and 6 indices for a plane
    glBufferData(GL_ARRAY_BUFFER, 4* sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

    // Set the vertex attribute pointers
    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) 0);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, Normal));
    // Vertex Texture Coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, TexCoords));

    glBindVertexArray(0);


}

Panel::~Panel() {
}
/**
 *
 * @param in - shader that was set to use !before! this call
 */

void Panel::draw(Shader* shader) {

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture.id);
    glUniform1i(glGetUniformLocation(shader->Program, "texture_diffuse1"), 0);

    // draw mesh
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);

    // Always good practice to set everything back to defaults once configured.
    for (GLuint i = 0; i < 1; i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }


}

void Panel::setActive(bool in) {

    /**
     * It is assumed that the texture provides a passive form in the lower
     * half and a active form in the upper half
     */
    if(!in) {
        isActive=true;
        vertices[0].TexCoords.y = 0.5;
        vertices[1].TexCoords.y = 0.0;
        vertices[2].TexCoords.y = 0.5;
        vertices[3].TexCoords.y = 0.0;
    }else{
        isActive=false;
        vertices[0].TexCoords.y = 1.0;
        vertices[1].TexCoords.y = 0.5;
        vertices[2].TexCoords.y = 1.0;
        vertices[3].TexCoords.y = 0.5;
    }

    glBindVertexArray(this->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

    // We know that we will always use 4 vertices and 6 indices for a plane
    glBufferData(GL_ARRAY_BUFFER, 4* sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

    glBindVertexArray(0);

}
