//
// Created by Knork on 07.08.2016.
//

#ifndef CP16_PANEL_H
#define CP16_PANEL_H

#include <string>
#include <vector>
#include <GL/glew.h>
#include "../../helper/MeshStructs.h"
#include "../../helper/Window.h"
#include "../../helper/Shader.h"

class Panel {

    /**
     * active Texture: tex coords y = 0.5-1
     * inactive Texture: tex coords y = 0.0-0.5
     */
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    /// We are NOT the owner of this pointer
    Window* const  window;

    Texture texture;
    bool isActive;

    GLuint VAO,EBO,VBO;

    void fillVertices();

public:
    /**
     *
     * @param path : to the texture for the img
     * @param x    : x-Pos 0-1.0 depenend on the window width
     * @param y    : y-Pos 0-1.0 depenend on the window height
     * @param size : size 0-1.0 depenend on the window height
     * @param ratio aspect ratio of the texture
     * @return
     */
    Panel(std::string path,glm::vec3 pos, GLfloat size, GLfloat ratio,Window* win);
    ~Panel();

    void draw(Shader *in);

    void setActive(bool in);

};


#endif //CP16_PANEL_H
