//
// Created by Knork on 19.08.2016.
//

#ifndef CP16_STRINGRENDERER_H
#define CP16_STRINGRENDERER_H


#include <GL/glew.h>
#include<glm/glm.hpp>
#include "../../helper/Window.h"
#include "../../helper/Shader.h"
#include <map>

class StringRenderer {

    Window *window;
    glm::vec4 color;
    std::string text;

    struct Character {
        GLuint TextureID;   // ID handle of the glyph texture
        glm::ivec2 Size;    // Size of glyph
        glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
        GLuint Advance;    // Horizontal offset to advance to next glyph
    };

    GLuint VAO,VBO;
    Shader shader;

    std::map<GLchar, Character> Characters;
public:
    StringRenderer(Window *win);
    void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec4 color);
    void RenderTextEvenSpacing(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec4 color);

};


#endif //CP16_STRINGRENDERER_H
