//
// Created by Knork on 03.07.2016.
//

#ifndef CP16_STAGEMANAGER_H
#define CP16_STAGEMANAGER_H

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

#define GLM_FORCE_RADIANS



#include "../helper/PlayerModel.h"
#include "../helper/Camera.h"

#include "../game/generation/TunnelSegment.h"
#include "../game/gameLoop/Player.h"


#include "../game/gameLoop/InputHandler.h"
#include "../helper/DrawAxis.h"
#include "../game/generation/TunnelGeneration.h"

#include "../game/StateManager.h"




#include "../helper/Window.h"
#include "states/GameState.h"

#include "../helper/GLOBAL_VARS.h"
#include "states/MenuState.h"
#include "states/LevelCompleteState.h"

/**
 * Handles what states is currently used
 * also sets the deltaTime
 *
 *
 */
class StateManager {

    Window *window;
//    GameState * const gameState;
    GameState * gameState;
    MenuState * menuState;
    LevelCompleteState * highScoreState;

    State * curState;


    GLfloat lastFrame,deltaTime;

    void drawLoadingScreen();

public:
    const static GLuint MENU=0;
    const static GLuint GAME=1;
    const static GLuint SCORE=2;
    const static GLuint PLAYERWON=3;
    const static GLuint PLAYERLOST=4;
    const static GLuint RETRY=5;
    const static GLuint NEXT=6;
    const static GLuint RESTART=7;

    /// init Phase
    StateManager();
    ~StateManager();
    /// gameLoop
    void gameLoop();

    void setDeltaTime();

    void checkStateChange();

    void setNextState(GLuint state);




};

#endif //CP16_STAGEMANAGER_H
