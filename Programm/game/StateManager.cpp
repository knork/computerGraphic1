//
// Created by Knork on 07.08.2016.
//

#include "StateManager.h"
#include "../helper/ASSERT.h"

void StateManager::gameLoop(){

    /// This keeps the Window open and running
    while (!glfwWindowShouldClose(window->getWindow())) {
        ASSERT(curState!=NULL,"curState in StateManager is Null");
        setDeltaTime();
        curState->update();
    }
}

StateManager::~StateManager() {

    delete game::stringRenderer;
    delete highScoreState;
    delete menuState;
    delete gameState;
//    delete window;

}

void StateManager::setDeltaTime() {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    game::deltaTime = deltaTime;
}

void StateManager::drawLoadingScreen() {
    glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    Shader unlitShader ("shader/vunlitTexture.glsl","shader/funlitTexture.glsl");
    unlitShader.use();
    Panel bg ("model/textures/menu/loading.png",glm::vec3(0.0f,1.0f,0.0f),1.00f,(1214.0f)/(683.0f),window);


    glm::mat4 proj = glm::ortho(0.0f,(float)(window->screenWidth),0.0f,(float)window->screenHeight,-1000.0f,1000.0f);
    glUniformMatrix4fv(glGetUniformLocation(unlitShader.Program,"proj"),1,GL_FALSE,&proj[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(unlitShader.Program,"view"),1,GL_FALSE,&(glm::mat4(1.0f))[0][0]);
    bg.draw(&unlitShader);

    glfwSwapBuffers(window->getWindow());
}

StateManager::StateManager() {
    game::stageManager=this;
    window = new Window();
    game::stringRenderer = new StringRenderer(window);

    drawLoadingScreen();

    menuState = new MenuState(window);
    gameState = new GameState(window);
    highScoreState = new LevelCompleteState(window);

    curState= menuState;
    curState->setActive();
//    curState= gameState;
    gameLoop();


}

void StateManager::setNextState(GLuint state) {

    switch (state) {
        case GAME:
            gameState->restart();
            curState=gameState;
            gameState->setActive();
            break;
        case MENU:
            curState=menuState;
            menuState->setActive();
            break;
        case PLAYERWON:
            curState=highScoreState;
            highScoreState->setGameOver(false);
            highScoreState->setActive();
            break;
        case PLAYERLOST:
            curState=highScoreState;
            highScoreState->setGameOver(true);
            highScoreState->setActive();
            break;

        case RETRY:
            gameState->retry();
            break;

        case NEXT:
            gameState->next();
            gameState->setActive();
            curState=gameState;
            break;
        case RESTART:
            gameState->restart();
            gameState->setActive();
            curState=gameState;
            break;
    }

}
