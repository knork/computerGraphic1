#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in mat4 instanceMatrix;

// sits at index 0
layout (std140) uniform Matrices{
    mat4 projection; // 4x 16 bytes
    mat4 view; // 4x 16bytes
    mat4 projView; // 4x 16 bytes
    vec3 viewPos; // 4 Bytes
    // Total 208 Bytes
};


out VS_OUT{
    vec3 FragPos; // world coords
    vec2 TexCoords;
} vs_out;


void main() {

    gl_Position = projection * view * instanceMatrix * vec4(position, 1.0f);
//       gl_Position = proj * view * instanceMatrix * vec4(position, 1.0f);
    vs_out.TexCoords = texCoords;
    // World Coords
    vs_out.FragPos = ( instanceMatrix * vec4(position,1.0f)).xyz;


}
