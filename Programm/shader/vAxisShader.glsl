#version 330 core


layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 col;

out vec3 fColor;

layout (std140) uniform Matrices{
    mat4 projection; // 4x 16 bytes
    mat4 view; // 4x 16bytes
    mat4 projView; // 4x 16 bytes
    // Total 192 Bytes
};

void main() {
    gl_Position = projection * view * vec4(pos, 1.0f);
    fColor = col;

}
