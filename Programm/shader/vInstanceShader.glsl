#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in mat4 instanceMatrix;
layout (location = 7) in vec3 tangent;
// 3,4,5,6

//out vec2 TexCoords;

// sits at index 0
layout (std140) uniform Matrices{
    mat4 projection; // 4x 16 bytes
    mat4 view; // 4x 16bytes
    mat4 projView; // 4x 16 bytes
    vec3 viewPos; // 4 Bytes
    // Total 208 Bytes
};

out VS_OUT{
    vec3 FragPos; // eye coords
    vec3 norm; // world coords
    vec2 TexCoords;
    vec3 viewDir;
    mat3 TBN;
} vs_out;




void main()
{
    gl_Position = projection * view * instanceMatrix * vec4(position, 1.0f);
//       gl_Position = proj * view * instanceMatrix * vec4(position, 1.0f);
    vs_out.TexCoords = texCoords;
    // World Coords


    vs_out.FragPos = ( instanceMatrix * vec4(position,1.0f)).xyz;
    vs_out.norm = (inverse(transpose(instanceMatrix))*vec4(normal,0.0f)).xyz;

    // calc TBN

    vec3 T = normalize(vec3(instanceMatrix * vec4(tangent, 0.0)));
    vec3 N = normalize(vec3(instanceMatrix * vec4(normal, 0.0)));
    // re-orthogonalize T with respect to N
    T = normalize(T - dot(T, N) * N);
    // then retrieve perpendicular vector B with the cross product of T and N
    vec3 B = cross(T, N);

    vs_out.TBN = mat3(T, B, N);


}