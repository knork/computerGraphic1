
#define NDEBUG

#define GLM_FORCE_RADIANS


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "helper/Window.h"
#include "helper/Camera.h"

#include "game/generation/TunnelSegment.h"
#include "game/gameLoop/Player.h"

#include <iostream>
#include "game/StateManager.h"


using namespace std;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

Camera *camera;
Player *player;
bool keys[1024];

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;


// Moves/alters the camera positions based on user input
void Do_Movement()
{
    // Camera controls
    if(keys[GLFW_KEY_W]) {
        camera->processKeyboard(FORWARD, deltaTime);


    }
    if(keys[GLFW_KEY_S]) {
        camera->processKeyboard(BACKWARD, deltaTime);

    }
    if(keys[GLFW_KEY_A]) {
        camera->processKeyboard(LEFT, deltaTime);
    }
    if(keys[GLFW_KEY_D]) {
        camera->processKeyboard(RIGHT, deltaTime);
    }
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{


    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    camera->processMouseMovement(xoffset, yoffset);
}



int main() {

    StateManager stateManager;
//
////    std::cout << "here" << std::endl;
//
////     inits glfw and glew ALWAYS the first thing
//    Window w;
//    camera = new Camera(glm::vec3(0.0f, 8.0f, 0.0f),glm::vec3(0.0,1.0,0.0), 0, 0, (float)w.screenWidth/ (float)w.screenHeight);
//    ObstacleLookUp olu;
//    camera->lookAt(glm::vec3(0.0f,0.0f,1.0f));
//    PlayerCollisionManager pc(1.2f,&olu);
//    glfwSetKeyCallback(w.getWindow(),key_callback);
//    InputHandler::InputHandler(&pc,&w,camera);
//
//    player= new Player("model/player/Player.obj",camera,&pc);
//
//    DrawAxis da;
//
//
//    TunnelGeneration tg(&pc,&olu);
//
//
//
//
//    while (!glfwWindowShouldClose(w.getWindow())) {
//
//        // Set frame time
//        GLfloat currentFrame = glfwGetTime();
//        deltaTime = currentFrame - lastFrame;
//        lastFrame = currentFrame;
//
//        glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//
//
//        game::deltaTime = deltaTime;
//        camera->updateMatricies();
//
//        InputHandler::processInputs();
//        pc.doCollsion();
//
//
//        player->draw();
//
//        tg.draw();
//
//
//        da.draw();
//
//        glfwPollEvents();
//        glfwSwapBuffers(w.getWindow());
//    }
//
//    delete( camera);
//    delete player;
    return 0;
}