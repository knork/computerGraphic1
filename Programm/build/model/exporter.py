import bpy, bmesh 
import os

def writeEmptyTags( f ):
    f.write("<meta>\n")
    f.write("<difficulty>0</difficulty>\n")
    f.write("<type>0</type>\n")
    f.write("<rot>0</rot>\n")
    f.write("</meta>\n")
    return;

def printStart( f , ob):
    print(ob.name)    
    print(ob.matrix_world)
    f.write("<start>\n")
    writeMat4( f , ob.matrix_world)
    f.write("</start>\n")
    return;

def printNext( f , ob):
    print(ob.name)    
    print(ob.matrix_world)
    f.write("<next>\n")
    writeMat4( f , ob.matrix_world)
    f.write("</next>\n")
    return;

def writePlane( f, ob ):
     
    print(ob.name,ob.matrix_world)
    f.write("<plane>\n")
    f.write("<transform>\n")
    writeMat4( f , ob.matrix_world )    
    f.write("</transform>\n")
    mesh = ob.data

    vertices = []

    for vert in mesh.vertices:
        f.write("<vec3><x>"+str(vert.co.x)+"</x><y>"+str(vert.co.y)+"</y><z>"+str(vert.co.z)+"</z></vec3>\n")
        
    indices = []
    for face in mesh.polygons:
        for ind in face.vertices:
            indices.append(ind)
    f.write("</plane>\n")   
    return;

def writeMat4( f , m ):
    #f.write("\t<mat4> \n")
    
    for vert in m:
        f.write("\t\t<vec4>")
        f.write("<x>" + str(vert.x) + "</x>")
        f.write("<y>" + str(vert.y) + "</y>")
        f.write("<z>" + str(vert.z) + "</z>")
        f.write("<w>" + str(vert.w) + "</w>")
        f.write("</vec4>\n")
    
    #f.write("\t</mat4> \n")
    return;

def writeVec3( f , m ):
#    f.write("<x>"+str(vert.co.x)+"</x><y>"+str(vert.co.y)+"</y><z>"+str(vert.co.z)+"</z>\n")
    return;

def writeMesh( f , ob ):
    f.write("<mesh>\n")
    f.write("<transform>\n")
    writeMat4( f , ob.matrix_world )
    
    
    mesh = ob.data

    normals = []
    indices = []
    for face in mesh.polygons:
        indices.append(face.vertices[0])
        indices.append(face.vertices[1])
        indices.append(face.vertices[2])
        for i in range(len(face.vertices)):
            v = mesh.vertices[face.vertices[i]]
            normals.append([v.normal[0],v.normal[1],v.normal[2]])

    verts = []
    for vert in mesh.vertices:
        verts.append(vert.co)

    uvs = []
    for uv_layer in mesh.uv_layers:
        for x in range(len(uv_layer.data)):
            uvs.append(uv_layer.data[x].uv)
            
    f.write("<indices>")
    for i in range(len(indices)):
        f.write("<i>"+ str(indices[i])+"</i>")
    f.write("</indices>\n")
    
    for i in range (len(verts)):
        
        f.write("<vertex>\n")
        f.write("<vec3 type=\"position\"><x>"+str(verts[i].x)+"</x><y>"+str(verts[i].y)+"</y><z>"+str(verts[i].z)+"</z></vec3>\n")
        f.write("<vec3 type=\"normal\"><x>"+str(normals[i][0])+"</x><y>"+str(normals[i][1])+"</y><z>"+str(normals[i][2])+"</z></vec3>\n")
        if(len(uvs) >=1 ):
            f.write("<vec2 type=\"uv\"><x>"+str(uvs[i][0])+"</x><y>"+str(uvs[i][1])+"</y></vec2>")
            
    
        
    
    


    return;


fname = bpy.path.basename(bpy.data.filepath)
fname = os.path.splitext(fname)[0]
fname = fname + ".xml"

f = open( fname, "w" )
f.write( "<TunnelSegment> \n")

writeEmptyTags( f ) 

# for every mesh we find

for ob in bpy.context.scene.objects: 
    
	
	
    if ob.name.find('_start') != -1:
       printStart( f , ob )
    
    if ob.name.find('_next') != -1:
       printNext( f , ob ) 
    # ignore cameras and such
    if ob.type != 'MESH': 
        continue
   
    if ob.name.find('_cP') != -1:
        writePlane(f , ob)
    # no custom quallifier -> mesh
    writeMesh(f , ob )

f.write( "</TunnelSegment>")
f.close()