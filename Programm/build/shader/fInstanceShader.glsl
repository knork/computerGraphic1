#version 330 core

layout (std140) uniform Matrices{
                    // Base align     // aligned offset
    mat4 projection; // 4x16          // 0
    mat4 view; // 4x 16bytes          // 64
    mat4 projView; // 4x 16 bytes     // 128
    vec3 viewPos; // 16 Bytes
    // Total 208 Bytes
};


in VS_OUT{
    vec3 FragPos; // world coords
    vec3 norm;
    vec2 TexCoords;
	vec3 viewDir;
	mat3 TBN;
} fs_in;

out vec4 color;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal;
uniform sampler2D texture_depth;



#define MAX 30


struct Light{
    vec3 ambient; // 16
    vec3 position; // 16
    int type;  //
    float spec; // 16
    // Total 48 Bytes
};
// these sit at index 1
layout (std140) uniform Lights{
int amount; // 4 Bytes ... somehow it's actually 16 ?!? why ? dunno
    Light lights[MAX]; // 30 * 40 = 1200
    // Total 1204 Bytes
};

#define strength 10.0f;

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{
    // number of depth layers
    const float minLayers = 10;
    const float maxLayers = 20;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy / viewDir.z * strength;
    vec2 deltaTexCoords = P / numLayers;

    // get initial values
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture(texture_depth, currentTexCoords).r;

    while(currentLayerDepth < currentDepthMapValue)
    {
        // shift texture coordinates along direction of P
        currentTexCoords -= deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = texture(texture_depth, currentTexCoords).r;
        // get depth of next layer
        currentLayerDepth += layerDepth;
    }

    // -- parallax occlusion mapping interpolation from here on
    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(texture_depth, prevTexCoords).r - currentLayerDepth + layerDepth;

    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}


vec3 pointLight(int index,vec3 norm, vec3 normFragPos){

    // light dir is in eye coords
//    vec3 lightDir = (view*vec4(lights[index].position,1.0)).xyz-fs_in.FragPos;
    vec3 lightDir = -(fs_in.FragPos - lights[index].position);
    float d = length(lightDir);
    lightDir = normalize ( lightDir);

//    lightDir = normalize ( fs_in.TBN *lightDir);
    vec3 amb = lights[index].ambient * vec3(texture(texture_diffuse1,fs_in.TexCoords));

    float diff = max(dot(lightDir,norm ), 0.0);
    vec3 diffuse = diff *vec3(texture(texture_diffuse1,fs_in.TexCoords));


    // direction to the viewer
    vec3 viewDir = normalize( fs_in.TBN *(viewPos - fs_in.FragPos));
    vec3 reflectDir = reflect(-lightDir,norm);
    float specAm = pow(max(dot(viewDir, reflectDir), 0.0),120);
    vec3 specular =   specAm * (vec3(texture(texture_specular1,fs_in.TexCoords)));


    vec3 ret =  ((amb/(2*amount)+diffuse+2*specular)/(0.8*d*d));
    return 20*ret;
}

void main()
{

    vec2 texCoords = fs_in.TexCoords;
//    texCoords = ParallaxMapping(fs_in.TexCoords,  fs_in.TBN * fs_in.viewDir);
//
//    // discards a fragment when sampling outside default texture region (fixes border artifacts)
//    if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0)
//        discard;

    // normalize Vars

//    vec3 norm = normalize(fs_in.norm);
    vec3 norm;

    norm = texture(texture_normal, fs_in.TexCoords).rgb;
    norm = normalize(norm * 2.0 - 1.0);
    norm = normalize(fs_in.TBN * norm);


    vec3 normFragPos = normalize(fs_in.FragPos);

//    vec4 tmp = vec4(texture(texture_diffuse1, fs_in.TexCoords));
    vec3 tmp;

    for(int i =0;i<MAX;i++){
        if(i<amount)
            tmp += pointLight(i,norm,normFragPos) ;
    }



// doesn' work... why?

//    float viewerDistance = length(fs_in.FragPos - viewPos);
//
//    tmp.x = tmp.x/((1000+viewerDistance)/1000);
//    tmp.y = tmp.y/((2000+viewerDistance)/2000);
//    tmp.z = tmp.z/((4000+viewerDistance)/4000);

    color = vec4(tmp,1.0);

}


